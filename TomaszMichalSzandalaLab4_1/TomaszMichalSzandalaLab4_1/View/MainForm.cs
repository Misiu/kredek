﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TomaszMichalSzandalaLab4_1.Model;

namespace TomaszMichalSzandalaLab4_1
{
    public partial class MainForm : Form
    {

        LINQConnectorDataContext linqConnector = new LINQConnectorDataContext();

        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonGetCustomers_Click(object sender, EventArgs e)
        {
            // var query = from customer in linqConnector.Customers select customer;
            dataGridViewMain.DataSource = Customer.getAll(linqConnector);
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            var clientsVar = from customer in linqConnector.Customers where customer.City == "Buenos Aires" select customer;
            foreach (var clientVar in clientsVar)
                clientVar.City = "Wroclaw";

            Customer client = (from customer in linqConnector.Customers select customer).Where(o => o.City == "Buenos Aires").First();

            linqConnector.SubmitChanges();

        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            #region magiczny trik
            //Sprawdzam czy coś tutaj działa, czy nie
            #endregion

            Customer customer = new Customer();
            customer.CompanyName = "NOKIA";
            customer.City = "SriLanka";
            customer.ContactName = "Stala";
            customer.CustomerID = textBoxID.Text;
            customer.Fax = "57";

            linqConnector.Customers.InsertOnSubmit(customer);

            linqConnector.SubmitChanges();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Customer client = (from customer in linqConnector.Customers select customer).
                    Where(o => o.CustomerID == textBoxID.Text).First();

                linqConnector.Customers.DeleteOnSubmit(client);
                linqConnector.SubmitChanges();
            }
            catch (Exception) { }

        }

        private void buttonGetMore_Click(object sender, EventArgs e)
        {
            var query = from customer in linqConnector.Customers
                        join order in linqConnector.Orders on customer.CustomerID equals order.CustomerID
                        select customer;
            dataGridViewMain.DataSource = query;
        }
    }
}

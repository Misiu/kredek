﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace TomaszMichałSzandałaLab3
{
    public partial class JoinUs : Form
    {
        SqlConnection sqlConnection;
        SqlDataAdapter sqlDataAdapter;
        int question;

        public JoinUs(int _question)
        {
            InitializeComponent();
            question = _question;
            sqlConnection = new SqlConnection("Data Source=localhost;database=WSI;Trusted_Connection=yes");
            prepareQuestion();
        }

        private void prepareQuestion()
        {

            sqlDataAdapter = new SqlDataAdapter("Select * from Question WHERE ID=" + question, sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);

            textBoxQuestion.Text = dataTable.Select()[0][1].ToString();
            dataTable.Clear();
            sqlDataAdapter = new SqlDataAdapter("Select * from Answer WHERE Question=" + question, sqlConnection);
            sqlDataAdapter.Fill(dataTable);
            button1.Text = dataTable.Select()[0][2].ToString();
            button2.Text = dataTable.Select()[1][2].ToString();
            button3.Text = dataTable.Select()[2][2].ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (buttonSolver(1))
                button1.BackColor = Color.Green;
            else
                button1.BackColor = Color.Red;
        
        }
    

        private void button2_Click(object sender, EventArgs e)
        {
            if (buttonSolver(2))
                button2.BackColor = Color.Green;
            else
                button2.BackColor = Color.Red;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (buttonSolver(3))
                button3.BackColor = Color.Green;
            else
                button3.BackColor = Color.Red;
        }

        private void buttonGo_Click(object sender, EventArgs e)
        {
            if (question < 4) { 
            JoinUs joinUs = new JoinUs(question + 1);
            joinUs.Show();
            }
            else
            {
                ApplicationForm application = new ApplicationForm();
                application.Show();
                this.Close();
            }
            this.Close();
        }


        private bool buttonSolver(int answerNumber)
        {
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);

            buttonGo.Enabled = true;
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;

            this.Refresh();

            if (dataTable.Select()[answerNumber-1][2].ToString() == "True")
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
    }
}

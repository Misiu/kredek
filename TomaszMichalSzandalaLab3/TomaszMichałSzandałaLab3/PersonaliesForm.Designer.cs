﻿namespace TomaszMichałSzandałaLab3
{
    partial class PersonaliesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.buttonOldMan = new System.Windows.Forms.Button();
            this.buttonLieutanants = new System.Windows.Forms.Button();
            this.buttonMeat = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView
            // 
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Location = new System.Drawing.Point(12, 105);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.Size = new System.Drawing.Size(675, 273);
            this.dataGridView.TabIndex = 0;
            // 
            // buttonOldMan
            // 
            this.buttonOldMan.Location = new System.Drawing.Point(78, 47);
            this.buttonOldMan.Name = "buttonOldMan";
            this.buttonOldMan.Size = new System.Drawing.Size(75, 23);
            this.buttonOldMan.TabIndex = 1;
            this.buttonOldMan.Text = "Starzy";
            this.buttonOldMan.UseVisualStyleBackColor = true;
            this.buttonOldMan.Click += new System.EventHandler(this.buttonOldMan_Click);
            // 
            // buttonLieutanants
            // 
            this.buttonLieutanants.Location = new System.Drawing.Point(289, 47);
            this.buttonLieutanants.Name = "buttonLieutanants";
            this.buttonLieutanants.Size = new System.Drawing.Size(75, 23);
            this.buttonLieutanants.TabIndex = 2;
            this.buttonLieutanants.Text = "Poruczniki";
            this.buttonLieutanants.UseVisualStyleBackColor = true;
            this.buttonLieutanants.Click += new System.EventHandler(this.button2_Click);
            // 
            // buttonMeat
            // 
            this.buttonMeat.Location = new System.Drawing.Point(525, 47);
            this.buttonMeat.Name = "buttonMeat";
            this.buttonMeat.Size = new System.Drawing.Size(104, 23);
            this.buttonMeat.TabIndex = 3;
            this.buttonMeat.Text = "Mięso armatnie";
            this.buttonMeat.UseVisualStyleBackColor = true;
            // 
            // PersonaliesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 390);
            this.Controls.Add(this.buttonMeat);
            this.Controls.Add(this.buttonLieutanants);
            this.Controls.Add(this.buttonOldMan);
            this.Controls.Add(this.dataGridView);
            this.Name = "PersonaliesForm";
            this.Text = "PersonaliesForm";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Button buttonOldMan;
        private System.Windows.Forms.Button buttonLieutanants;
        private System.Windows.Forms.Button buttonMeat;
    }
}
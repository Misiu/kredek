﻿namespace TomaszMichałSzandałaLab3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonEnemies = new System.Windows.Forms.Button();
            this.buttonOurs = new System.Windows.Forms.Button();
            this.buttonJoin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonEnemies
            // 
            this.buttonEnemies.Location = new System.Drawing.Point(65, 44);
            this.buttonEnemies.Name = "buttonEnemies";
            this.buttonEnemies.Size = new System.Drawing.Size(141, 74);
            this.buttonEnemies.TabIndex = 0;
            this.buttonEnemies.Text = "Wrogowie";
            this.buttonEnemies.UseVisualStyleBackColor = true;
            this.buttonEnemies.Click += new System.EventHandler(this.buttonEnemies_Click);
            // 
            // buttonOurs
            // 
            this.buttonOurs.Location = new System.Drawing.Point(65, 150);
            this.buttonOurs.Name = "buttonOurs";
            this.buttonOurs.Size = new System.Drawing.Size(141, 74);
            this.buttonOurs.TabIndex = 1;
            this.buttonOurs.Text = "Nasi";
            this.buttonOurs.UseVisualStyleBackColor = true;
            this.buttonOurs.Click += new System.EventHandler(this.buttonOurs_Click);
            // 
            // buttonJoin
            // 
            this.buttonJoin.Location = new System.Drawing.Point(65, 246);
            this.buttonJoin.Name = "buttonJoin";
            this.buttonJoin.Size = new System.Drawing.Size(141, 74);
            this.buttonJoin.TabIndex = 2;
            this.buttonJoin.Text = "Dołącz do nas!";
            this.buttonJoin.UseVisualStyleBackColor = true;
            this.buttonJoin.Click += new System.EventHandler(this.buttonJoin_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 363);
            this.Controls.Add(this.buttonJoin);
            this.Controls.Add(this.buttonOurs);
            this.Controls.Add(this.buttonEnemies);
            this.Name = "Form1";
            this.Text = "Menu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonEnemies;
        private System.Windows.Forms.Button buttonOurs;
        private System.Windows.Forms.Button buttonJoin;
    }
}


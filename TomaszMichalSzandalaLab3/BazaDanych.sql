USE [WSI]
GO
/****** Object:  Table [dbo].[Threat]    Script Date: 05/18/2015 23:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Threat](
	[ID] [int] NOT NULL,
	[ShortThreat] [nvarchar](50) NULL,
	[Description] [nvarchar](100) NULL,
 CONSTRAINT [PK_Threat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Threat] ([ID], [ShortThreat], [Description]) VALUES (1, N'Bez zagrożeń', N'Nie stanowi zagrożenia')
INSERT [dbo].[Threat] ([ID], [ShortThreat], [Description]) VALUES (2, N'Małe zagrożenie', N'Nakopać, by przemyślał')
INSERT [dbo].[Threat] ([ID], [ShortThreat], [Description]) VALUES (3, N'Niepokojący obiekt', N'Ośmieszać, dyskredytować')
INSERT [dbo].[Threat] ([ID], [ShortThreat], [Description]) VALUES (4, N'Duże zagrożenie', N'Doprowadzić do utraty zaufania osób trzecich')
INSERT [dbo].[Threat] ([ID], [ShortThreat], [Description]) VALUES (5, N'Krytyczne zagrożenie', N'Zlikwidować gdy nadarzy się okazja')
/****** Object:  Table [dbo].[Specialisation]    Script Date: 05/18/2015 23:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Specialisation](
	[ID] [int] NOT NULL,
	[Specialisation] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Specialisation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Specialisation] ([ID], [Specialisation]) VALUES (1, N'Szantaż')
INSERT [dbo].[Specialisation] ([ID], [Specialisation]) VALUES (2, N'Wymuszenia')
INSERT [dbo].[Specialisation] ([ID], [Specialisation]) VALUES (3, N'Pobicia')
INSERT [dbo].[Specialisation] ([ID], [Specialisation]) VALUES (4, N'Morderstwa')
INSERT [dbo].[Specialisation] ([ID], [Specialisation]) VALUES (5, N'Bukmacherka')
INSERT [dbo].[Specialisation] ([ID], [Specialisation]) VALUES (6, N'Kradzieże')
INSERT [dbo].[Specialisation] ([ID], [Specialisation]) VALUES (7, N'Zastraszanie')
INSERT [dbo].[Specialisation] ([ID], [Specialisation]) VALUES (8, N'Podsłuchy')
/****** Object:  Table [dbo].[Lieutenant]    Script Date: 05/18/2015 23:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lieutenant](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
	[NickName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Lieutenant] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Lieutenant] ([ID], [Name], [Surname], [NickName]) VALUES (1, N'Juliusz', N'Tobiasz', NULL)
INSERT [dbo].[Lieutenant] ([ID], [Name], [Surname], [NickName]) VALUES (2, N'Jarek', N'Piesar', N'Pieseł')
INSERT [dbo].[Lieutenant] ([ID], [Name], [Surname], [NickName]) VALUES (3, N'Marek', N'Draczyński', N'Draco')
/****** Object:  Table [dbo].[Question]    Script Date: 05/18/2015 23:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question](
	[ID] [int] NOT NULL,
	[Question] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Question] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Question] ([ID], [Question]) VALUES (1, N'Z czego mają żyć polacy wg Bronisława Komorowskiego?')
INSERT [dbo].[Question] ([ID], [Question]) VALUES (2, N'Kto jest największym partnerem handlowym Polski w UE, wg Bronisława Komorowskiego?')
INSERT [dbo].[Question] ([ID], [Question]) VALUES (3, N'Czego nie robi prezydent RP, wg Pana Prezydenta RP, Bronisłąwa Komorowskiego?')
/****** Object:  Table [dbo].[OldMan]    Script Date: 05/18/2015 23:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OldMan](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
	[Occupation] [nvarchar](50) NULL,
 CONSTRAINT [PK_OldMan] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[OldMan] ([ID], [Name], [Surname], [Occupation]) VALUES (1, N'Bronisław', N'Komorowski', N'Prezydent RP')
INSERT [dbo].[OldMan] ([ID], [Name], [Surname], [Occupation]) VALUES (2, N'Andrzej', N'Lichocki', N'Biznesmen')
INSERT [dbo].[OldMan] ([ID], [Name], [Surname], [Occupation]) VALUES (3, N'Leszek', N'Tobiasz', N'Pułkownik')
INSERT [dbo].[OldMan] ([ID], [Name], [Surname], [Occupation]) VALUES (4, N'Sylwester', N'Latkowski', N'Redaktor')
INSERT [dbo].[OldMan] ([ID], [Name], [Surname], [Occupation]) VALUES (5, N'Aleksander', N'Lichocki', N'Pułkownik')
INSERT [dbo].[OldMan] ([ID], [Name], [Surname], [Occupation]) VALUES (6, N'Tadeusz', N'Rusiak', N'Generał')
/****** Object:  Table [dbo].[Meat]    Script Date: 05/18/2015 23:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Meat](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
	[NickName] [nvarchar](50) NOT NULL,
	[Commander] [int] NOT NULL,
 CONSTRAINT [PK_Meat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Meat] ([ID], [Name], [Surname], [NickName], [Commander]) VALUES (1, N'Gwidon', N'Biały', N'Murzyn', 1)
INSERT [dbo].[Meat] ([ID], [Name], [Surname], [NickName], [Commander]) VALUES (2, N'Robert', N'Motyka', N'Grabie', 1)
INSERT [dbo].[Meat] ([ID], [Name], [Surname], [NickName], [Commander]) VALUES (3, N'Damian', N'Wasyl', N'Wasyl', 2)
/****** Object:  Table [dbo].[Enemy]    Script Date: 05/18/2015 23:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Enemy](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Surname] [nvarchar](50) NULL,
	[Occupation] [nvarchar](50) NULL,
	[Threat] [int] NULL,
	[Status] [nvarchar](50) NULL,
 CONSTRAINT [PK_Enemies] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Enemy] ([ID], [Name], [Surname], [Occupation], [Threat], [Status]) VALUES (1, N'Wojciech', N'Sumliński', N'Dziennikarz', 5, N'Aktywny')
INSERT [dbo].[Enemy] ([ID], [Name], [Surname], [Occupation], [Threat], [Status]) VALUES (2, N'Antoni ', N'Macierewicz', N'Likwidator WSI', 3, N'Mało aktywny')
INSERT [dbo].[Enemy] ([ID], [Name], [Surname], [Occupation], [Threat], [Status]) VALUES (3, N'Lech', N'Kaczyński', N'Prezydent', 1, N'Wącha kwiatki do spodu')
/****** Object:  Table [dbo].[Answer]    Script Date: 05/18/2015 23:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Answer](
	[ID] [int] NOT NULL,
	[Answer] [nvarchar](50) NOT NULL,
	[Correct] [bit] NOT NULL,
	[Question] [int] NOT NULL,
 CONSTRAINT [PK_Answer] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Answer] ([ID], [Answer], [Correct], [Question]) VALUES (1, N'Z przyzwczajenia', 1, 1)
INSERT [dbo].[Answer] ([ID], [Answer], [Correct], [Question]) VALUES (2, N'Z zarobionych pieniędzy', 0, 1)
INSERT [dbo].[Answer] ([ID], [Answer], [Correct], [Question]) VALUES (3, N'Z dnia na dzień', 0, 1)
INSERT [dbo].[Answer] ([ID], [Answer], [Correct], [Question]) VALUES (4, N'Niemcy', 0, 2)
INSERT [dbo].[Answer] ([ID], [Answer], [Correct], [Question]) VALUES (5, N'Rosja', 0, 2)
INSERT [dbo].[Answer] ([ID], [Answer], [Correct], [Question]) VALUES (6, N'Stany Zjednoczone', 1, 2)
INSERT [dbo].[Answer] ([ID], [Answer], [Correct], [Question]) VALUES (7, N'Prezydent nie poluje', 0, 3)
INSERT [dbo].[Answer] ([ID], [Answer], [Correct], [Question]) VALUES (8, N'Prezydent nie podpisuje budżetu', 1, 3)
INSERT [dbo].[Answer] ([ID], [Answer], [Correct], [Question]) VALUES (9, N'Prezydent nie zmienia zdania', 0, 3)
/****** Object:  View [dbo].[Quiz]    Script Date: 05/18/2015 23:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Quiz]
AS
SELECT     dbo.Question.Question, dbo.Answer.Answer, dbo.Answer.Correct
FROM         dbo.Answer INNER JOIN
                      dbo.Question ON dbo.Answer.Question = dbo.Question.ID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Answer"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Question"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 153
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Quiz'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Quiz'
GO
/****** Object:  View [dbo].[EnemyNThreat]    Script Date: 05/18/2015 23:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[EnemyNThreat]
AS
SELECT     dbo.Enemy.Name, dbo.Enemy.Surname, dbo.Enemy.Status, dbo.Threat.Description
FROM         dbo.Enemy INNER JOIN
                      dbo.Threat ON dbo.Enemy.Threat = dbo.Threat.ID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Enemy"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 168
               Right = 195
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Threat"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 110
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'EnemyNThreat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'EnemyNThreat'
GO
/****** Object:  Table [dbo].[Meat_Specialisation]    Script Date: 05/18/2015 23:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Meat_Specialisation](
	[ID] [int] NOT NULL,
	[MeatID] [int] NOT NULL,
	[SpecialisationID] [int] NOT NULL,
 CONSTRAINT [PK_Meat_Specialisation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Meat_Specialisation] ([ID], [MeatID], [SpecialisationID]) VALUES (1, 1, 2)
INSERT [dbo].[Meat_Specialisation] ([ID], [MeatID], [SpecialisationID]) VALUES (2, 3, 1)
INSERT [dbo].[Meat_Specialisation] ([ID], [MeatID], [SpecialisationID]) VALUES (3, 1, 1)
INSERT [dbo].[Meat_Specialisation] ([ID], [MeatID], [SpecialisationID]) VALUES (4, 2, 5)
INSERT [dbo].[Meat_Specialisation] ([ID], [MeatID], [SpecialisationID]) VALUES (5, 2, 7)
INSERT [dbo].[Meat_Specialisation] ([ID], [MeatID], [SpecialisationID]) VALUES (6, 1, 8)
INSERT [dbo].[Meat_Specialisation] ([ID], [MeatID], [SpecialisationID]) VALUES (7, 2, 3)
/****** Object:  View [dbo].[MeatDat]    Script Date: 05/18/2015 23:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MeatDat]
AS
SELECT     dbo.Lieutenant.NickName AS CommanderNickName, dbo.Meat.NickName, dbo.Meat.Surname, dbo.Meat.Name, dbo.Specialisation.Specialisation
FROM         dbo.Meat_Specialisation INNER JOIN
                      dbo.Meat ON dbo.Meat_Specialisation.MeatID = dbo.Meat.ID INNER JOIN
                      dbo.Lieutenant ON dbo.Meat.Commander = dbo.Lieutenant.ID INNER JOIN
                      dbo.Specialisation ON dbo.Meat_Specialisation.SpecialisationID = dbo.Specialisation.ID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Meat_Specialisation"
            Begin Extent = 
               Top = 6
               Left = 434
               Bottom = 110
               Right = 598
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Lieutenant"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 221
               Right = 238
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Meat"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 163
               Right = 394
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Specialisation"
            Begin Extent = 
               Top = 6
               Left = 636
               Bottom = 95
               Right = 796
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MeatDat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MeatDat'
GO
/****** Object:  ForeignKey [FK_Answer_Question]    Script Date: 05/18/2015 23:38:37 ******/
ALTER TABLE [dbo].[Answer]  WITH CHECK ADD  CONSTRAINT [FK_Answer_Question] FOREIGN KEY([Question])
REFERENCES [dbo].[Question] ([ID])
GO
ALTER TABLE [dbo].[Answer] CHECK CONSTRAINT [FK_Answer_Question]
GO
/****** Object:  ForeignKey [FK_Enemy_Threat]    Script Date: 05/18/2015 23:38:37 ******/
ALTER TABLE [dbo].[Enemy]  WITH CHECK ADD  CONSTRAINT [FK_Enemy_Threat] FOREIGN KEY([Threat])
REFERENCES [dbo].[Threat] ([ID])
GO
ALTER TABLE [dbo].[Enemy] CHECK CONSTRAINT [FK_Enemy_Threat]
GO
/****** Object:  ForeignKey [FK_Meat_Lieutenant]    Script Date: 05/18/2015 23:38:37 ******/
ALTER TABLE [dbo].[Meat]  WITH CHECK ADD  CONSTRAINT [FK_Meat_Lieutenant] FOREIGN KEY([Commander])
REFERENCES [dbo].[Lieutenant] ([ID])
GO
ALTER TABLE [dbo].[Meat] CHECK CONSTRAINT [FK_Meat_Lieutenant]
GO
/****** Object:  ForeignKey [FK_Meat_Meat]    Script Date: 05/18/2015 23:38:37 ******/
ALTER TABLE [dbo].[Meat]  WITH CHECK ADD  CONSTRAINT [FK_Meat_Meat] FOREIGN KEY([ID])
REFERENCES [dbo].[Meat] ([ID])
GO
ALTER TABLE [dbo].[Meat] CHECK CONSTRAINT [FK_Meat_Meat]
GO
/****** Object:  ForeignKey [FK_Meat_Specialisation_Meat]    Script Date: 05/18/2015 23:38:37 ******/
ALTER TABLE [dbo].[Meat_Specialisation]  WITH CHECK ADD  CONSTRAINT [FK_Meat_Specialisation_Meat] FOREIGN KEY([MeatID])
REFERENCES [dbo].[Meat] ([ID])
GO
ALTER TABLE [dbo].[Meat_Specialisation] CHECK CONSTRAINT [FK_Meat_Specialisation_Meat]
GO
/****** Object:  ForeignKey [FK_Meat_Specialisation_Specialisation]    Script Date: 05/18/2015 23:38:37 ******/
ALTER TABLE [dbo].[Meat_Specialisation]  WITH CHECK ADD  CONSTRAINT [FK_Meat_Specialisation_Specialisation] FOREIGN KEY([SpecialisationID])
REFERENCES [dbo].[Specialisation] ([ID])
GO
ALTER TABLE [dbo].[Meat_Specialisation] CHECK CONSTRAINT [FK_Meat_Specialisation_Specialisation]
GO
/****** Object:  ForeignKey [FK_Threat_Threat]    Script Date: 05/18/2015 23:38:37 ******/
ALTER TABLE [dbo].[Threat]  WITH CHECK ADD  CONSTRAINT [FK_Threat_Threat] FOREIGN KEY([ID])
REFERENCES [dbo].[Threat] ([ID])
GO
ALTER TABLE [dbo].[Threat] CHECK CONSTRAINT [FK_Threat_Threat]
GO

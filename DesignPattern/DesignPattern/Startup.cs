﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DesignPattern.Startup))]
namespace DesignPattern
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DesignPattern.Models;
using DesignPattern.Models.ModelForView;

namespace DesignPattern.Controllers
{
    public class PizzasController : Controller
    {
        MyDBContext dbContext = new MyDBContext();

        // GET: Pizzas
        public ActionResult Index()
        {
            List<PizzaForView> pizzasForView = new List<PizzaForView>();
            PizzaForView pizzaForView = new PizzaForView();

            var pizzas = (from pizza in dbContext.Pizzas select pizza);
            foreach (Pizza pizza in pizzas)
            {
                pizzaForView = new PizzaForView();
                pizzaForView.pizza = pizza;

                var ingredients = (from pizzaIngredient in dbContext.PizzaIngredients
                                   join ingr in dbContext.Ingredients on pizzaIngredient.Ingredient.ID equals ingr.ID 
                                   where pizzaIngredient.Pizza.ID == pizza.ID select pizzaIngredient);

                foreach (var ingredient in ingredients)
                {
                    //var ingr = (from ingredientInDB in dbContext.Ingredients where ingredientInDB.ID == ingredient.Ingredient.ID select ingredientInDB);
                    //try
                    //{
                    pizzaForView.ingredients.Add(ingredient.Ingredient);
                    //}
                    //catch (Exception) { }

                }
                pizzasForView.Add(pizzaForView);
            }
            return View(pizzasForView);
        }
    }
}
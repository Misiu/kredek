﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DesignPattern.Models.ModelForView
{
    public class PizzaForView
    {
        public Pizza pizza;
        public List<Ingredient> ingredients;

        public PizzaForView()
        {
            ingredients = new List<Ingredient>();
        }
        public double getValue()
        {
            double sum = 0.0;
            foreach (var ingredient in ingredients)
            {
                sum += ingredient.Value;
            }
            return sum;
        }
        public string getIngredientNames()
        {
            string litOfNames = "";
            foreach (var ingredient in ingredients)
            {
                litOfNames += ingredient.Name+",";
            }
            return litOfNames;
        }
    }
}
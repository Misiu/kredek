﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace DesignPattern.Models
{
    public class Pizza
    {
        public int ID { set; get; }
        public string Name { set; get; }
        
    }
}
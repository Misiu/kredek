﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace DesignPattern.Models
{
    public class MyDBInitializer : DropCreateDatabaseAlways<MyDBContext>
    {
        protected override void Seed(MyDBContext dbContext){
            #region skladniki
            Ingredient placek = new Ingredient { Name = "Placek", Value = 10.00 };
            Ingredient sos = new Ingredient { Name = "Sos pomidorowy", Value = 0.50 };
            Ingredient ser = new Ingredient { Name = "Mozzarella", Value = 1.00 };
            Ingredient salami = new Ingredient { Name = "Salami", Value = 2.00 };
            Ingredient czosnek = new Ingredient { Name = "Czosnek", Value = 1.00 };
            Ingredient papryka = new Ingredient { Name = "Papryka", Value = 1.50 };
            Ingredient pomidory = new Ingredient { Name = "Suszone pomidory", Value = 2.00 };
            Ingredient oregano = new Ingredient { Name = "Oregano", Value = 0.00 };
            Ingredient plesniak = new Ingredient { Name = "Ser pleśniowy", Value = 1.00 };
            Ingredient sosCzosnokowy = new Ingredient { Name = "Sos czosnkowy", Value = 0.50 };
            Ingredient sosOstry = new Ingredient { Name = "Sos ostry", Value = 0.50 };
            dbContext.Ingredients.Add(placek);
            dbContext.Ingredients.Add(sos);
            dbContext.Ingredients.Add(ser);
            dbContext.Ingredients.Add(salami);
            dbContext.Ingredients.Add(czosnek);
            dbContext.Ingredients.Add(papryka);
            dbContext.Ingredients.Add(pomidory);
            dbContext.Ingredients.Add(oregano);
            dbContext.Ingredients.Add(plesniak);
            dbContext.Ingredients.Add(sosCzosnokowy);
            dbContext.Ingredients.Add(sosOstry);
            dbContext.SaveChanges();
            #endregion

            PizzasIngredients pi;

            #region pizze
            Pizza pizza = new Pizza();
            pizza.Name = "Margherita";
            dbContext.Pizzas.Add(pizza);
            pi=new PizzasIngredients{Pizza=pizza, Ingredient=placek};
            dbContext.PizzaIngredients.Add(pi);
             pi=new PizzasIngredients{Pizza=pizza, Ingredient=sos};
            dbContext.PizzaIngredients.Add(pi);
             pi=new PizzasIngredients{Pizza=pizza, Ingredient=ser};
            dbContext.PizzaIngredients.Add(pi);


            pizza = new Pizza();
            pizza.Name = "Salame";
             dbContext.Pizzas.Add(pizza);
            pi=new PizzasIngredients{Pizza=pizza, Ingredient=placek};
            dbContext.PizzaIngredients.Add(pi);
             pi=new PizzasIngredients{Pizza=pizza, Ingredient=sos};
            dbContext.PizzaIngredients.Add(pi);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = ser };
            dbContext.PizzaIngredients.Add(pi);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = salami };
            dbContext.PizzaIngredients.Add(pi);
       


            pizza = new Pizza();
            pizza.Name = "Vardensia";
            dbContext.Pizzas.Add(pizza);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = placek };
            dbContext.PizzaIngredients.Add(pi);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = sos };
            dbContext.PizzaIngredients.Add(pi);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = ser };
            dbContext.PizzaIngredients.Add(pi);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = salami };
            dbContext.PizzaIngredients.Add(pi);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = papryka };
            dbContext.PizzaIngredients.Add(pi);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = pomidory };
            dbContext.PizzaIngredients.Add(pi);



            pizza = new Pizza();
            pizza.Name = "Magiczna";
            dbContext.Pizzas.Add(pizza);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = placek };
            dbContext.PizzaIngredients.Add(pi);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = sos };
            dbContext.PizzaIngredients.Add(pi);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = ser };
            dbContext.PizzaIngredients.Add(pi);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = salami };
            dbContext.PizzaIngredients.Add(pi);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = czosnek };
            dbContext.PizzaIngredients.Add(pi);

            pizza = new Pizza();
            pizza.Name = "Czarcia";
            dbContext.Pizzas.Add(pizza);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = placek };
            dbContext.PizzaIngredients.Add(pi);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = sos };
            dbContext.PizzaIngredients.Add(pi);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = ser };
            dbContext.PizzaIngredients.Add(pi);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = oregano };
            dbContext.PizzaIngredients.Add(pi);
            pi = new PizzasIngredients { Pizza = pizza, Ingredient = czosnek };
            dbContext.PizzaIngredients.Add(pi);
            #endregion

            dbContext.SaveChanges();
            base.Seed(dbContext);
        }
    }
}
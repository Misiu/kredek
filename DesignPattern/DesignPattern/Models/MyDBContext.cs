﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace DesignPattern.Models
{
    public class MyDBContext: DbContext
    {
        public MyDBContext(): base("stringer") {

            Database.SetInitializer<MyDBContext>(new MyDBInitializer());

        }
        public virtual DbSet<Pizza> Pizzas{set; get;}
        public virtual DbSet<Ingredient> Ingredients { set; get; }
        public virtual DbSet<PizzasIngredients> PizzaIngredients { set; get; }
    }
}
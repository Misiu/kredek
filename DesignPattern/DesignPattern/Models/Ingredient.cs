﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DesignPattern.Models
{
    public class Ingredient
    {
        public int ID { set; get; }
        public string Name { set; get; }
        public double Value { set; get; }
    }
}
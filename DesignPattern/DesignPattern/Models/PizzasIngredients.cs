﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DesignPattern.Models
{
    public class PizzasIngredients
    {
        public int ID { set; get; }
        public Pizza Pizza { set; get; }
        public Ingredient Ingredient { set; get; }
    }
}
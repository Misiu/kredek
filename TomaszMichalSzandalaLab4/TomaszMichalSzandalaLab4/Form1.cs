﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TomaszMichalSzandalaLab4
{
    public partial class Form1 : Form
    {
        TutorialLINQClassDataContext dataClass = new TutorialLINQClassDataContext();
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonTest1_Click(object sender, EventArgs e)
        {
            var query = from c in dataClass.Products select c;
            dataGridView.DataSource = query;

        }

        private void buttonTest2_Click(object sender, EventArgs e)
        {
            var query = from c in dataClass.Products select new { c.ProductID, c.ProductName };
            dataGridView.DataSource = query;
        }

        private void buttonTest3_Click(object sender, EventArgs e)
        {
            var query = from c in dataClass.Products orderby c.ProductID descending select new { c.ProductID, c.ProductName };
            dataGridView.DataSource = query;
        }

      
    }
}

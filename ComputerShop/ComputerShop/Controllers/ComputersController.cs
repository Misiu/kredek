﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ComputerShop.Models;
using ComputerShop.Models.LocalModels;
using ComputerShop.Models.ModelsForViews;
using ComputerShop.Controllers;
using ComputerShop.Controllers.Builder;

namespace ComputerShop.Controllers
{
    public class ComputersController : Controller
    {
        MyDBContext dbContext = new MyDBContext();

        public ActionResult Index()
        {
            List<Computer> listOfComputers = new List<Computer>();
            ComputerKeeper computerKeeper=ComputerKeeper.getSelf();

            foreach(var computer in computerKeeper.getProducts()){
                listOfComputers.Add((Computer)computer.Value);
            }
            return View(listOfComputers);
        }

        public ActionResult createNewProduct()
        {
            var procesors = from procesor in dbContext.Procesors select procesor;
            var ramCubes = from ramCube in dbContext.RAMCubes select ramCube;
            var graphicCards = from graphiCard in dbContext.GraphicCards select graphiCard;

            PartsContainer partsContainer = new PartsContainer { procesorList = procesors.ToList(), graphicCardList = graphicCards.ToList(), ramCubeList = ramCubes.ToList() };

            return View(partsContainer);
        }
        [HttpPost]
        public ActionResult assembleProduct()
        {
            
            int procesorID=Convert.ToInt32(Request.Form["Procesor"]);
            int graphicCardID = Convert.ToInt32(Request.Form["GraphicCard"]);
            int ramID = Convert.ToInt32(Request.Form["RAM"]);

            ComputerBuilder computerBuilder = new ComputerBuilder();
            computerBuilder.procesor = (from procesor in dbContext.Procesors where procesor.ID == procesorID select procesor).FirstOrDefault();
            computerBuilder.graphicCard = (from graphicCard in dbContext.GraphicCards where graphicCard.ID == graphicCardID select graphicCard).FirstOrDefault();
            computerBuilder.ramCube = (from ramCube in dbContext.RAMCubes where ramCube.ID == ramID select ramCube).FirstOrDefault();

            Product product = (Computer)computerBuilder.createProduct();
            ComputerKeeper computerKeeper = ComputerKeeper.getSelf();
            computerKeeper.addNewComputer(product);

            return RedirectToAction("Index");
        }


        public ActionResult getRandomPromotion(){
            Random random = new Random();
            double newPromotion=random.NextDouble()*0.5+0.5;
            ComputerKeeper computerKeeper = ComputerKeeper.getSelf();

            computerKeeper.notifyObservers(newPromotion);
            return RedirectToAction("Index");
        }
    
    }
}
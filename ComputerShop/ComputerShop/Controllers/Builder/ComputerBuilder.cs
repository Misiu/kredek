﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ComputerShop.Models.LocalModels;
using ComputerShop.Models;

namespace ComputerShop.Controllers.Builder
{
    public class ComputerBuilder: BuilderInterface
    {
        public RAMCube ramCube;
        public Procesor procesor;
        public GraphicCard graphicCard;

        public Product createProduct()
        {
            Computer computer = new Computer();
            
            computer.procesor = procesor;
            computer.ramCube = ramCube;
            computer.graphicCard = graphicCard;
            computer.identifier = createIdentifier();

            return (Product)computer;
        }

        public string createIdentifier()
        {
            return procesor.Name + ramCube.Name + graphicCard.Name;
        }

 
    }
}
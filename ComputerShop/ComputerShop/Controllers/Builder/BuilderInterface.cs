﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ComputerShop.Models.LocalModels;
using ComputerShop.Models;

namespace ComputerShop.Controllers.Builder
{
    /// <summary>
    /// Interface for future builders.
    /// Since fields and methods of the interface are always public I skip the keyword.
    /// </summary>
    interface BuilderInterface
    {
        Product createProduct();
        string createIdentifier();
        
    }
}

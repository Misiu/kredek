﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ComputerShop.Models;
namespace ComputerShop.Models.ModelsForViews
{
    public class PartsContainer
    {
       public  List<Procesor> procesorList { set; get; }
       public  List<GraphicCard> graphicCardList { set; get; }
       public List<RAMCube> ramCubeList { set; get; }
    }
}
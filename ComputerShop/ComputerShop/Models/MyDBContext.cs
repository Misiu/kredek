﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;
namespace ComputerShop.Models
{
    public class MyDBContext : DbContext
    {
        public MyDBContext(): base("stringer")
        {

            Database.SetInitializer<MyDBContext>(new MyDBInitializer());

        }
        public virtual DbSet<Procesor> Procesors { set; get; }
        public virtual DbSet<RAMCube> RAMCubes { set; get; }
        public virtual DbSet<GraphicCard> GraphicCards { set; get; }
    }
}
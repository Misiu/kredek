﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComputerShop.Models
{
    public class BaseObject
    {
        public int ID { set; get; }
        public string Name { set; get; }
        public int Value { set; get; }
        public override string ToString()
        {
            return Name;
        }
    }

}
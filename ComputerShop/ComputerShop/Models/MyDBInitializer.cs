﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ComputerShop.Models
{
    public class MyDBInitializer:    DropCreateDatabaseAlways<MyDBContext>
    {
        protected override void Seed(MyDBContext dbContext){
            #region Parts
            dbContext.Procesors.Add(new Procesor { Name = "i3", Value = 300 });
            dbContext.Procesors.Add(new Procesor { Name = "i5", Value = 500 });
            dbContext.Procesors.Add(new Procesor { Name = "i7", Value = 700 });

            dbContext.RAMCubes.Add(new RAMCube { Name = "4GB", Value = 100 });
            dbContext.RAMCubes.Add(new RAMCube { Name = "8GB", Value = 200 });
            dbContext.RAMCubes.Add(new RAMCube { Name = "16GB", Value = 400 });
            dbContext.RAMCubes.Add(new RAMCube { Name = "32GB", Value = 800 });

            dbContext.GraphicCards.Add(new GraphicCard { Name = "GTX 850", Value = 600 });
            dbContext.GraphicCards.Add(new GraphicCard { Name = "GTX 860", Value = 800 });
            dbContext.GraphicCards.Add(new GraphicCard { Name = "GTX 870", Value = 400 });
            dbContext.GraphicCards.Add(new GraphicCard { Name = "GTX 950", Value = 700 });
            dbContext.GraphicCards.Add(new GraphicCard { Name = "GTX 960", Value = 1100 });
            #endregion

            dbContext.SaveChanges();
            base.Seed(dbContext);
        }
    }
}
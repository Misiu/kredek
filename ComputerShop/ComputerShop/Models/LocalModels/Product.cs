﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComputerShop.Models.LocalModels
{
    public class Product: Observator
    {
        public string identifier { set; get; }

        public double promotion { set; get; }
        public void update(double newPromotion)
        {
            promotion = newPromotion;
        }
    }
}
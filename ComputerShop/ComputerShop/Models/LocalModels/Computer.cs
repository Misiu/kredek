﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComputerShop.Models.LocalModels
{
    public class Computer: Product
    {
        public Procesor procesor { set; get; }
        public RAMCube ramCube { set; get; }
        public GraphicCard graphicCard { set; get; }
        

        public Computer()
        {
            promotion = 1.0;
        }

        public int getPrice()
        {
            return (int)((procesor.Value + graphicCard.Value + ramCube.Value)*promotion);
        }

       
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComputerShop.Models.LocalModels
{
    /// <summary>
    /// The Singleton class.
    /// I assume that non-specified fields and ethods are private, so I skipped all "private" keywords
    /// </summary>
    public class ComputerKeeper
    {
        #region singleton issues
        static volatile ComputerKeeper self;
        /// <summary>
        /// securing for multithreading
        /// </summary>
        private static object syncRoot = new Object();

        #region useless contructor
        ComputerKeeper()
        { }
        #endregion

        public static ComputerKeeper getSelf()
        {
            if (self == null)
            {
                lock (syncRoot)
                {
                    self = new ComputerKeeper();
                }
            }
            return self;
        }
        #endregion

        #region flyweight pattern dictionary
        /// <summary>
        /// Flyweight pattern.
        /// Implementation of the flyweight bases on dictionary, which if have specified object,
        /// returns it, otherwise: it creates new one and adds to dictionary
        /// </summary>
        private Dictionary<string, Product> products = new Dictionary<string, Product>();
        public void addNewComputer(Product _product)
        {
            if (products.ContainsKey(_product.identifier))
            {
                return;
            }
            products.Add(_product.identifier, _product);
            observators.Add(products[_product.identifier]);
        }

        public Dictionary<string, Product> getProducts()
        {
            return products;
        }
        #endregion
        #region observator issues
        List<Observator> observators = new List<Observator>();
        public void notifyObservers(double newPromotion){
            foreach (var observator in observators)
                observator.update(newPromotion);
        }
        #endregion
    }
}
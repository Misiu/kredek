﻿using Pizzeria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pizzeria.Controllers
{
    public class PizzaController : Controller
    {
        //
        // GET: /Pizza/
        DBContext dbContext = new DBContext();

        public ActionResult Index()
        {
            var pizzas = (from pizza in dbContext.Pizzas select pizza);


            return View(pizzas.ToList());
        }

        

        /// <summary>
        /// Method to create basic database content
        /// </summary>
        /// <returns>Nothing here</returns>
        public ActionResult FillDatabase()
        {
            #region skladniki
            Ingredient placek = new Ingredient { Name = "Placek", Value = 10.00 };
            Ingredient sos = new Ingredient { Name = "Sos pomidorowy", Value = 0.50 };
            Ingredient ser = new Ingredient { Name = "Mozzarella", Value = 1.00 };
            Ingredient salami = new Ingredient { Name = "Salami", Value = 2.00 };
            Ingredient czosnek = new Ingredient { Name = "Czosnek", Value = 1.00 };
            Ingredient papryka = new Ingredient { Name = "Papryka", Value = 1.50 };
            Ingredient pomidory = new Ingredient { Name = "Suszone pomidory", Value = 2.00 };
            Ingredient oregano = new Ingredient { Name = "Oregano", Value = 0.00 };
            Ingredient plesniak = new Ingredient { Name = "Ser pleśniowy", Value = 1.00 };
            dbContext.Ingredients.Add(placek);
            dbContext.Ingredients.Add(sos);
            dbContext.Ingredients.Add(ser);
            dbContext.Ingredients.Add(salami);
            dbContext.Ingredients.Add(czosnek);
            dbContext.Ingredients.Add(papryka);
            dbContext.Ingredients.Add(pomidory);
            dbContext.Ingredients.Add(oregano);
            dbContext.Ingredients.Add(plesniak);
            #endregion


            #region pizze
            Pizza pizza = new Pizza();
            pizza.Name = "Margherita";
            pizza.Ingredients.Add(placek);
            //pizza.addIngredient(placek);
            //pizza.addIngredient(sos);
            //pizza.addIngredient(ser);
            dbContext.Pizzas.Add(pizza);

            //pizza = new Pizza();
            //pizza.Name = "Salame";
            //pizza.addIngredient(placek);
            //pizza.addIngredient(sos);
            //pizza.addIngredient(ser);
            //pizza.addIngredient(salami);
            //dbContext.Pizzas.Add(pizza);

            //pizza = new Pizza();
            //pizza.Name = "Vardensia";
            //pizza.addIngredient(placek);
            //pizza.addIngredient(sos);
            //pizza.addIngredient(ser);
            //pizza.addIngredient(salami);
            //pizza.addIngredient(papryka);
            //pizza.addIngredient(pomidory);
            //dbContext.Pizzas.Add(pizza);

            //pizza = new Pizza();
            //pizza.Name = "Magiczna";
            //pizza.addIngredient(placek);
            //pizza.addIngredient(sos);
            //pizza.addIngredient(ser);
            //pizza.addIngredient(salami);
            //pizza.addIngredient(czosnek);
            //dbContext.Pizzas.Add(pizza);

            //pizza = new Pizza();
            //pizza.Name = "Czarcia";
            //pizza.addIngredient(placek);
            //pizza.addIngredient(sos);
            //pizza.addIngredient(ser);
            //pizza.addIngredient(oregano);
            //pizza.addIngredient(czosnek);
            //pizza.addIngredient(salami);
            //dbContext.Pizzas.Add(pizza);
            #endregion

            dbContext.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pizzeria.Models
{
    public class Pizza: IDMaker
    {
        
        public string Name{set; get;}
        public virtual ICollection<Ingredient> Ingredients {set; get;}

        //public void addIngredient(Ingredient ingredient)
        //{
        //    if (Ingredients == null)
        //        Ingredients = new ICollection<Ingredient>();
        //    Ingredients.Add(ingredient);
        //}
        /// <summary>
        /// sums the value of ingredients
        /// </summary>
        /// <returns>Value of pizza</returns>
        public double getValue()
        {
            double sum=0.0;
            foreach (var ingredient in Ingredients)
                sum += ingredient.Value;
            return sum;
        }
        

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pizzeria.Models
{
    public class Ingredient: IDMaker
    {
        public string Name { set; get; }
        public double Value { set; get; }
    }
}
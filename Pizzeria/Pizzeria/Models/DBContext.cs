﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Pizzeria.Models
{
    public class DBContext : DbContext
    {
        public DBContext() : base("stringer") { }

        #region WTF is it??
        public virtual DbSet<Pizza> Pizzas { set; get; }
        public virtual DbSet<Ingredient> Ingredients { set; get; }
        #endregion
    }
}
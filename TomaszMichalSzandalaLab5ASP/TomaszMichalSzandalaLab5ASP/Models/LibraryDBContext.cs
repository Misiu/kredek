﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TomaszMichalSzandalaLab5ASP.Models
{
    public class LibraryDBContext: DbContext
    {
        public LibraryDBContext() : base("stringer") {
            Database.SetInitializer<LibraryDBContext>(new LibraryDbInitializer());
        }
        public virtual DbSet<Movie> Movie{set; get;}

    }
}
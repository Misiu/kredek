﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
namespace TomaszMichalSzandalaLab5ASP.Models
{
    public class LibraryDbInitializer:DropCreateDatabaseAlways<LibraryDBContext>
    {
       // private LibraryDBContext context = new LibraryDBContext();
        protected override void Seed(LibraryDBContext context)
        {
            context.Movie.Add(new Movie() { Name = "Pies", DateTime = DateTime.Now });
            context.Movie.Add(new Movie() { Name = "Thor", DateTime = DateTime.Now });
            context.Movie.Add(new Movie() { Name = "Thor 2", DateTime = DateTime.Now });
            context.Movie.Add(new Movie() { Name = "Jamnik", DateTime = DateTime.Now });
            context.SaveChanges();
            base.Seed(context);
        }
    }
}

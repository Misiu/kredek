﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TomaszMichalSzandalaLab5ASP.Models
{
    public class Movie
    {
        public int ID { set; get; }
        //[Display]
        public string Name { set; get; }
        public DateTime DateTime { set; get; }
    }
}
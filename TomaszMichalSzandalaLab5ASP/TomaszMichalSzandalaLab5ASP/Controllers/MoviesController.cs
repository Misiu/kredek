﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using TomaszMichalSzandalaLab5ASP.Models;

namespace TomaszMichalSzandalaLab5ASP.Controllers
{
    public class MoviesController : Controller
    {
        //
        // GET: /Movies/
        LibraryDBContext ldbcontext = new LibraryDBContext();


        public ActionResult Index()
        {

            var query = (from movie in ldbcontext.Movie select movie);
            return View(query.ToList());
        }
        public void DbInit()
        {
            ldbcontext.Movie.Add(new Movie() { Name = "Avengers", DateTime = DateTime.Now });
            ldbcontext.Movie.Add(new Movie() { Name = "Avengers 2", DateTime = DateTime.Now });
            ldbcontext.Movie.Add(new Movie() { Name = "Thor", DateTime = DateTime.Now });
            ldbcontext.Movie.Add(new Movie() { Name = "Thor 2", DateTime = DateTime.Now });
            ldbcontext.Movie.Add(new Movie() { Name = "Jamnik", DateTime = DateTime.Now });
            ldbcontext.Movie.Add(new Movie() { Name = "Pies", DateTime = DateTime.Now });
            ldbcontext.SaveChanges();
        }
        public ActionResult ShowAll()
        {
            var query = from movie in ldbcontext.Movie select movie;

            return View(query.ToList());
        }
        [HttpPost]
        public ActionResult AddNewMovie()
        {
            Movie _movie = new Movie();
            _movie.Name = "301";
            _movie.DateTime = DateTime.Now;
            ldbcontext.Movie.Add(_movie);
            ldbcontext.SaveChanges();

            return RedirectToAction("ShowAll");

        }
        [HttpPost]
        public string DeleteById(int movieId)
        {
            try
            {
                Movie movieToDelete = ldbcontext.Movie.Find(movieId);
                ldbcontext.Movie.Remove(movieToDelete);
                ldbcontext.SaveChanges();
            }
            catch (Exception exp)
            {
                return exp.Message;
            }

            return "0";

        }
       public ActionResult UpdateById(int id)
        {
            var query = from movie in ldbcontext.Movie select movie;

            return View("UpdateById");
        }
       [HttpPost]
       public ActionResult SaveChanges(Movie newMovieData)
       {
           Movie movie = ldbcontext.Movie.Find(newMovieData.ID);
           movie.Name = newMovieData.Name;

           movie.DateTime = newMovieData.DateTime;

           ldbcontext.SaveChanges();

           return RedirectToAction("ShowAll");

       }
    }
}
   
/*
 * 0-wyświtlić w tabeli
 * 1-Pizza Menu Tworzenie włąsnej pizzy
 * 
 */

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TomaszMichalSzandalaLab5.Model;

namespace TomaszMichalSzandalaLab5.View
{
    

    public partial class BoardForm : Form
    {
        List<TextBox> textBoxes = new List<TextBox>();
        int currentPlayer;
        int gameID;
        int player1, player2;

        public BoardForm(int _gameID)
        {
            gameID = _gameID;
            InitializeComponent();
            prepareTextBoxes();
        }
        public void setPlayers(int _player1, int _player2)
        {
            player1 = _player1;
            player2 = _player2;
            currentPlayer = player1;
        }

        private void buttonDice_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int nextJump = random.Next(1, 7);



            if (currentPlayer != player1)
            {
                currentPlayer = player1;
            }
            else
            {
                currentPlayer = player2;
            }

        }
        private void prepareBoard()
        {
            textBoxLog.Text += "\nTura gracza: " + Player.getName(currentPlayer);

        }

        #region private 'supporting' methods
        private void prepareTextBoxes()
        {
            textBoxes.Add(textBoxStart);
            textBoxes.Add(textBoxHamburg);
            textBoxes.Add(textBoxBerlin);
            textBoxes.Add(textBoxPunishment1);
            textBoxes.Add(textBoxNeapol);
            textBoxes.Add(textBoxRzym);
            textBoxes.Add(textBoxBarcelona);
            textBoxes.Add(textBoxRandom);

            textBoxes.Add(textBoxMadryt);
            textBoxes.Add(textBoxLiverpool);
            textBoxes.Add(textBoxPunishment2);
            textBoxes.Add(textBoxLondyn);
            textBoxes.Add(textBoxFree);

            textBoxes.Add(textBoxMarsylia);
            textBoxes.Add(textBoxParyz);
            textBoxes.Add(textBoxSaloniki);
            textBoxes.Add(textBoxPunishment3);
            textBoxes.Add(textBoxAteny);
            textBoxes.Add(textBoxPorto);
            textBoxes.Add(textBoxPunishment4);

            textBoxes.Add(textBoxLizbona);
            textBoxes.Add(textBoxZurych);
            textBoxes.Add(textBoxBerno);
            textBoxes.Add(textBoxSanMarino);
        }
        #endregion
    }
}

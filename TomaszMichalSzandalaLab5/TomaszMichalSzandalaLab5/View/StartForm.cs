﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TomaszMichalSzandalaLab5.Model;
using TomaszMichalSzandalaLab5.View;

namespace TomaszMichalSzandalaLab5
{
    public partial class StartForm : Form
    {
        public StartForm()
        {
            InitializeComponent();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            int gameID = Game.newGame();
            int player1ID = Player.newPlayer(gameID, textBoxPlayer1Name.Text);
            int player2ID = Player.newPlayer(gameID, textBoxPlayer2Name.Text);

           BoardForm boardForm= new BoardForm(gameID);
           boardForm.setPlayers(player1ID, player2ID);

           boardForm.Show();
           //this.Close();
        }

        
    }
}

﻿namespace TomaszMichalSzandalaLab5.View
{
    partial class BoardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxFree = new System.Windows.Forms.TextBox();
            this.textBoxLondyn = new System.Windows.Forms.TextBox();
            this.textBoxPunishment2 = new System.Windows.Forms.TextBox();
            this.textBoxLiverpool = new System.Windows.Forms.TextBox();
            this.textBoxMadryt = new System.Windows.Forms.TextBox();
            this.textBoxRandom = new System.Windows.Forms.TextBox();
            this.textBoxBarcelona = new System.Windows.Forms.TextBox();
            this.textBoxRzym = new System.Windows.Forms.TextBox();
            this.textBoxNeapol = new System.Windows.Forms.TextBox();
            this.textBoxPunishment1 = new System.Windows.Forms.TextBox();
            this.textBoxBerlin = new System.Windows.Forms.TextBox();
            this.textBoxHamburg = new System.Windows.Forms.TextBox();
            this.textBoxStart = new System.Windows.Forms.TextBox();
            this.textBoxSanMarino = new System.Windows.Forms.TextBox();
            this.textBoxBerno = new System.Windows.Forms.TextBox();
            this.textBoxZurych = new System.Windows.Forms.TextBox();
            this.textBoxLizbona = new System.Windows.Forms.TextBox();
            this.textBoxPunishment4 = new System.Windows.Forms.TextBox();
            this.textBoxParyz = new System.Windows.Forms.TextBox();
            this.textBoxMarsylia = new System.Windows.Forms.TextBox();
            this.textBoxPorto = new System.Windows.Forms.TextBox();
            this.textBoxSaloniki = new System.Windows.Forms.TextBox();
            this.textBoxPunishment3 = new System.Windows.Forms.TextBox();
            this.textBoxAteny = new System.Windows.Forms.TextBox();
            this.textBoxFreeParkingFee = new System.Windows.Forms.TextBox();
            this.buttonBuyHouse = new System.Windows.Forms.Button();
            this.buttonDice = new System.Windows.Forms.Button();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxFree
            // 
            this.textBoxFree.Location = new System.Drawing.Point(20, 50);
            this.textBoxFree.Multiline = true;
            this.textBoxFree.Name = "textBoxFree";
            this.textBoxFree.ReadOnly = true;
            this.textBoxFree.Size = new System.Drawing.Size(112, 72);
            this.textBoxFree.TabIndex = 0;
            this.textBoxFree.Text = "Darmowy Parking";
            // 
            // textBoxLondyn
            // 
            this.textBoxLondyn.Location = new System.Drawing.Point(20, 139);
            this.textBoxLondyn.Multiline = true;
            this.textBoxLondyn.Name = "textBoxLondyn";
            this.textBoxLondyn.ReadOnly = true;
            this.textBoxLondyn.Size = new System.Drawing.Size(112, 72);
            this.textBoxLondyn.TabIndex = 1;
            this.textBoxLondyn.Text = "Londyn";
            // 
            // textBoxPunishment2
            // 
            this.textBoxPunishment2.Location = new System.Drawing.Point(20, 236);
            this.textBoxPunishment2.Multiline = true;
            this.textBoxPunishment2.Name = "textBoxPunishment2";
            this.textBoxPunishment2.ReadOnly = true;
            this.textBoxPunishment2.Size = new System.Drawing.Size(112, 72);
            this.textBoxPunishment2.TabIndex = 2;
            this.textBoxPunishment2.Text = "Kara 300$";
            // 
            // textBoxLiverpool
            // 
            this.textBoxLiverpool.Location = new System.Drawing.Point(20, 327);
            this.textBoxLiverpool.Multiline = true;
            this.textBoxLiverpool.Name = "textBoxLiverpool";
            this.textBoxLiverpool.ReadOnly = true;
            this.textBoxLiverpool.Size = new System.Drawing.Size(112, 72);
            this.textBoxLiverpool.TabIndex = 3;
            this.textBoxLiverpool.Text = "Liverpool";
            // 
            // textBoxMadryt
            // 
            this.textBoxMadryt.Location = new System.Drawing.Point(20, 426);
            this.textBoxMadryt.Multiline = true;
            this.textBoxMadryt.Name = "textBoxMadryt";
            this.textBoxMadryt.ReadOnly = true;
            this.textBoxMadryt.Size = new System.Drawing.Size(112, 72);
            this.textBoxMadryt.TabIndex = 4;
            this.textBoxMadryt.Text = "Madryt";
            // 
            // textBoxRandom
            // 
            this.textBoxRandom.Location = new System.Drawing.Point(20, 532);
            this.textBoxRandom.Multiline = true;
            this.textBoxRandom.Name = "textBoxRandom";
            this.textBoxRandom.ReadOnly = true;
            this.textBoxRandom.Size = new System.Drawing.Size(112, 72);
            this.textBoxRandom.TabIndex = 5;
            this.textBoxRandom.Text = "Losowy skok";
            // 
            // textBoxBarcelona
            // 
            this.textBoxBarcelona.Location = new System.Drawing.Point(164, 532);
            this.textBoxBarcelona.Multiline = true;
            this.textBoxBarcelona.Name = "textBoxBarcelona";
            this.textBoxBarcelona.ReadOnly = true;
            this.textBoxBarcelona.Size = new System.Drawing.Size(112, 72);
            this.textBoxBarcelona.TabIndex = 6;
            this.textBoxBarcelona.Text = "Barcelona";
            // 
            // textBoxRzym
            // 
            this.textBoxRzym.Location = new System.Drawing.Point(307, 532);
            this.textBoxRzym.Multiline = true;
            this.textBoxRzym.Name = "textBoxRzym";
            this.textBoxRzym.ReadOnly = true;
            this.textBoxRzym.Size = new System.Drawing.Size(112, 72);
            this.textBoxRzym.TabIndex = 7;
            this.textBoxRzym.Text = "Rzym";
            // 
            // textBoxNeapol
            // 
            this.textBoxNeapol.Location = new System.Drawing.Point(463, 532);
            this.textBoxNeapol.Multiline = true;
            this.textBoxNeapol.Name = "textBoxNeapol";
            this.textBoxNeapol.ReadOnly = true;
            this.textBoxNeapol.Size = new System.Drawing.Size(112, 72);
            this.textBoxNeapol.TabIndex = 8;
            this.textBoxNeapol.Text = "Neapol";
            // 
            // textBoxPunishment1
            // 
            this.textBoxPunishment1.Location = new System.Drawing.Point(612, 532);
            this.textBoxPunishment1.Multiline = true;
            this.textBoxPunishment1.Name = "textBoxPunishment1";
            this.textBoxPunishment1.ReadOnly = true;
            this.textBoxPunishment1.Size = new System.Drawing.Size(112, 72);
            this.textBoxPunishment1.TabIndex = 9;
            this.textBoxPunishment1.Text = "Kara 400$";
            // 
            // textBoxBerlin
            // 
            this.textBoxBerlin.Location = new System.Drawing.Point(751, 532);
            this.textBoxBerlin.Multiline = true;
            this.textBoxBerlin.Name = "textBoxBerlin";
            this.textBoxBerlin.ReadOnly = true;
            this.textBoxBerlin.Size = new System.Drawing.Size(112, 72);
            this.textBoxBerlin.TabIndex = 10;
            this.textBoxBerlin.Text = "Berlin";
            // 
            // textBoxHamburg
            // 
            this.textBoxHamburg.Location = new System.Drawing.Point(880, 532);
            this.textBoxHamburg.Multiline = true;
            this.textBoxHamburg.Name = "textBoxHamburg";
            this.textBoxHamburg.ReadOnly = true;
            this.textBoxHamburg.Size = new System.Drawing.Size(112, 72);
            this.textBoxHamburg.TabIndex = 11;
            this.textBoxHamburg.Text = "Hamburg";
            // 
            // textBoxStart
            // 
            this.textBoxStart.Location = new System.Drawing.Point(1028, 532);
            this.textBoxStart.Multiline = true;
            this.textBoxStart.Name = "textBoxStart";
            this.textBoxStart.ReadOnly = true;
            this.textBoxStart.Size = new System.Drawing.Size(112, 72);
            this.textBoxStart.TabIndex = 12;
            this.textBoxStart.Text = "START";
            // 
            // textBoxSanMarino
            // 
            this.textBoxSanMarino.Location = new System.Drawing.Point(1028, 445);
            this.textBoxSanMarino.Multiline = true;
            this.textBoxSanMarino.Name = "textBoxSanMarino";
            this.textBoxSanMarino.ReadOnly = true;
            this.textBoxSanMarino.Size = new System.Drawing.Size(112, 72);
            this.textBoxSanMarino.TabIndex = 17;
            this.textBoxSanMarino.Text = "San Marino";
            // 
            // textBoxBerno
            // 
            this.textBoxBerno.Location = new System.Drawing.Point(1028, 346);
            this.textBoxBerno.Multiline = true;
            this.textBoxBerno.Name = "textBoxBerno";
            this.textBoxBerno.ReadOnly = true;
            this.textBoxBerno.Size = new System.Drawing.Size(112, 72);
            this.textBoxBerno.TabIndex = 16;
            this.textBoxBerno.Text = "Berno";
            // 
            // textBoxZurych
            // 
            this.textBoxZurych.Location = new System.Drawing.Point(1028, 255);
            this.textBoxZurych.Multiline = true;
            this.textBoxZurych.Name = "textBoxZurych";
            this.textBoxZurych.ReadOnly = true;
            this.textBoxZurych.Size = new System.Drawing.Size(112, 72);
            this.textBoxZurych.TabIndex = 15;
            this.textBoxZurych.Text = "Zurych";
            // 
            // textBoxLizbona
            // 
            this.textBoxLizbona.Location = new System.Drawing.Point(1028, 158);
            this.textBoxLizbona.Multiline = true;
            this.textBoxLizbona.Name = "textBoxLizbona";
            this.textBoxLizbona.ReadOnly = true;
            this.textBoxLizbona.Size = new System.Drawing.Size(112, 72);
            this.textBoxLizbona.TabIndex = 14;
            this.textBoxLizbona.Text = "Lizbona";
            // 
            // textBoxPunishment4
            // 
            this.textBoxPunishment4.Location = new System.Drawing.Point(1028, 50);
            this.textBoxPunishment4.Multiline = true;
            this.textBoxPunishment4.Name = "textBoxPunishment4";
            this.textBoxPunishment4.ReadOnly = true;
            this.textBoxPunishment4.Size = new System.Drawing.Size(112, 72);
            this.textBoxPunishment4.TabIndex = 13;
            this.textBoxPunishment4.Text = "Kara: 200$";
            // 
            // textBoxParyz
            // 
            this.textBoxParyz.Location = new System.Drawing.Point(336, 50);
            this.textBoxParyz.Multiline = true;
            this.textBoxParyz.Name = "textBoxParyz";
            this.textBoxParyz.ReadOnly = true;
            this.textBoxParyz.Size = new System.Drawing.Size(112, 72);
            this.textBoxParyz.TabIndex = 21;
            this.textBoxParyz.Text = "Paryż";
            // 
            // textBoxMarsylia
            // 
            this.textBoxMarsylia.Location = new System.Drawing.Point(182, 50);
            this.textBoxMarsylia.Multiline = true;
            this.textBoxMarsylia.Name = "textBoxMarsylia";
            this.textBoxMarsylia.ReadOnly = true;
            this.textBoxMarsylia.Size = new System.Drawing.Size(112, 72);
            this.textBoxMarsylia.TabIndex = 20;
            this.textBoxMarsylia.Text = "Marsylia";
            // 
            // textBoxPorto
            // 
            this.textBoxPorto.Location = new System.Drawing.Point(902, 50);
            this.textBoxPorto.Multiline = true;
            this.textBoxPorto.Name = "textBoxPorto";
            this.textBoxPorto.ReadOnly = true;
            this.textBoxPorto.Size = new System.Drawing.Size(112, 72);
            this.textBoxPorto.TabIndex = 25;
            this.textBoxPorto.Text = "Porto";
            // 
            // textBoxSaloniki
            // 
            this.textBoxSaloniki.Location = new System.Drawing.Point(489, 50);
            this.textBoxSaloniki.Multiline = true;
            this.textBoxSaloniki.Name = "textBoxSaloniki";
            this.textBoxSaloniki.ReadOnly = true;
            this.textBoxSaloniki.Size = new System.Drawing.Size(112, 72);
            this.textBoxSaloniki.TabIndex = 22;
            this.textBoxSaloniki.Text = "Saloniki";
            // 
            // textBoxPunishment3
            // 
            this.textBoxPunishment3.Location = new System.Drawing.Point(634, 50);
            this.textBoxPunishment3.Multiline = true;
            this.textBoxPunishment3.Name = "textBoxPunishment3";
            this.textBoxPunishment3.ReadOnly = true;
            this.textBoxPunishment3.Size = new System.Drawing.Size(112, 72);
            this.textBoxPunishment3.TabIndex = 23;
            this.textBoxPunishment3.Text = "Kara 400$";
            // 
            // textBoxAteny
            // 
            this.textBoxAteny.Location = new System.Drawing.Point(773, 50);
            this.textBoxAteny.Multiline = true;
            this.textBoxAteny.Name = "textBoxAteny";
            this.textBoxAteny.ReadOnly = true;
            this.textBoxAteny.Size = new System.Drawing.Size(112, 72);
            this.textBoxAteny.TabIndex = 24;
            this.textBoxAteny.Text = "Ateny";
            // 
            // textBoxFreeParkingFee
            // 
            this.textBoxFreeParkingFee.Location = new System.Drawing.Point(182, 139);
            this.textBoxFreeParkingFee.Name = "textBoxFreeParkingFee";
            this.textBoxFreeParkingFee.ReadOnly = true;
            this.textBoxFreeParkingFee.Size = new System.Drawing.Size(127, 20);
            this.textBoxFreeParkingFee.TabIndex = 26;
            this.textBoxFreeParkingFee.Text = "Za parking: 0$";
            // 
            // buttonBuyHouse
            // 
            this.buttonBuyHouse.Location = new System.Drawing.Point(836, 399);
            this.buttonBuyHouse.Name = "buttonBuyHouse";
            this.buttonBuyHouse.Size = new System.Drawing.Size(156, 72);
            this.buttonBuyHouse.TabIndex = 28;
            this.buttonBuyHouse.Text = "Kup dom";
            this.buttonBuyHouse.UseVisualStyleBackColor = true;
            // 
            // buttonDice
            // 
            this.buttonDice.Location = new System.Drawing.Point(836, 300);
            this.buttonDice.Name = "buttonDice";
            this.buttonDice.Size = new System.Drawing.Size(156, 72);
            this.buttonDice.TabIndex = 29;
            this.buttonDice.Text = "Rzuć monetą";
            this.buttonDice.UseVisualStyleBackColor = true;
            this.buttonDice.Click += new System.EventHandler(this.buttonDice_Click);
            // 
            // textBoxLog
            // 
            this.textBoxLog.Location = new System.Drawing.Point(182, 176);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.ReadOnly = true;
            this.textBoxLog.Size = new System.Drawing.Size(266, 311);
            this.textBoxLog.TabIndex = 30;
            this.textBoxLog.Text = "Gra rozpoczęta!";
            // 
            // BoardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1168, 629);
            this.Controls.Add(this.textBoxLog);
            this.Controls.Add(this.buttonDice);
            this.Controls.Add(this.buttonBuyHouse);
            this.Controls.Add(this.textBoxFreeParkingFee);
            this.Controls.Add(this.textBoxPorto);
            this.Controls.Add(this.textBoxAteny);
            this.Controls.Add(this.textBoxPunishment3);
            this.Controls.Add(this.textBoxSaloniki);
            this.Controls.Add(this.textBoxParyz);
            this.Controls.Add(this.textBoxMarsylia);
            this.Controls.Add(this.textBoxSanMarino);
            this.Controls.Add(this.textBoxBerno);
            this.Controls.Add(this.textBoxZurych);
            this.Controls.Add(this.textBoxLizbona);
            this.Controls.Add(this.textBoxPunishment4);
            this.Controls.Add(this.textBoxStart);
            this.Controls.Add(this.textBoxHamburg);
            this.Controls.Add(this.textBoxBerlin);
            this.Controls.Add(this.textBoxPunishment1);
            this.Controls.Add(this.textBoxNeapol);
            this.Controls.Add(this.textBoxRzym);
            this.Controls.Add(this.textBoxBarcelona);
            this.Controls.Add(this.textBoxRandom);
            this.Controls.Add(this.textBoxMadryt);
            this.Controls.Add(this.textBoxLiverpool);
            this.Controls.Add(this.textBoxPunishment2);
            this.Controls.Add(this.textBoxLondyn);
            this.Controls.Add(this.textBoxFree);
            this.Name = "BoardForm";
            this.Text = "BoardForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxFree;
        private System.Windows.Forms.TextBox textBoxLondyn;
        private System.Windows.Forms.TextBox textBoxPunishment2;
        private System.Windows.Forms.TextBox textBoxLiverpool;
        private System.Windows.Forms.TextBox textBoxMadryt;
        private System.Windows.Forms.TextBox textBoxRandom;
        private System.Windows.Forms.TextBox textBoxBarcelona;
        private System.Windows.Forms.TextBox textBoxRzym;
        private System.Windows.Forms.TextBox textBoxNeapol;
        private System.Windows.Forms.TextBox textBoxPunishment1;
        private System.Windows.Forms.TextBox textBoxBerlin;
        private System.Windows.Forms.TextBox textBoxHamburg;
        private System.Windows.Forms.TextBox textBoxStart;
        private System.Windows.Forms.TextBox textBoxSanMarino;
        private System.Windows.Forms.TextBox textBoxBerno;
        private System.Windows.Forms.TextBox textBoxZurych;
        private System.Windows.Forms.TextBox textBoxLizbona;
        private System.Windows.Forms.TextBox textBoxPunishment4;
        private System.Windows.Forms.TextBox textBoxParyz;
        private System.Windows.Forms.TextBox textBoxMarsylia;
        private System.Windows.Forms.TextBox textBoxPorto;
        private System.Windows.Forms.TextBox textBoxSaloniki;
        private System.Windows.Forms.TextBox textBoxPunishment3;
        private System.Windows.Forms.TextBox textBoxAteny;
        private System.Windows.Forms.TextBox textBoxFreeParkingFee;
        private System.Windows.Forms.Button buttonBuyHouse;
        private System.Windows.Forms.Button buttonDice;
        private System.Windows.Forms.TextBox textBoxLog;
    }
}
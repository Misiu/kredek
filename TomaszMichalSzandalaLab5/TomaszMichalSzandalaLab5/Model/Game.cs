﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TomaszMichalSzandalaLab5.Model
{
    public partial class Game
    {
        public static int newGame()
        {
            Game game = new Game();
            
            game.DateStarted = DateTime.Today;

            // dbm będzie się często przewijał, więc nazwa zmiennej go reprezentującej będzie "dbm"
            DBManagerDataContext dbm = new DBManagerDataContext();
            dbm.Games.InsertOnSubmit(game);
            dbm.SubmitChanges();

            return game.ID;

        }
    }
}

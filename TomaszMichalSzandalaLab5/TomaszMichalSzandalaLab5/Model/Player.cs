﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TomaszMichalSzandalaLab5.Model
{
    public partial class Player
    {
        // dbm będzie się często przewijał, więc nazwa zmiennej go reprezentującej będzie "dbm"
        static DBManagerDataContext dbm = new DBManagerDataContext();
        //TODO czy potrzebne?
        static int gameID;

        public static int newPlayer(int _gameID, string name)
        {
            Player player = new Player();
            player.Name = name;
            player.Game = _gameID;
            gameID = _gameID;

            
            dbm.Players.InsertOnSubmit(player);
            dbm.SubmitChanges();

            return player.ID;
        }

        public static string getName(int id)
        {
            Player tempPlayer=  (from player in dbm.Players select player).Where(o => o.ID == id ).First();
            return tempPlayer.Name;
        }
        public static int getPosition(int id)
        {
            Player tempPlayer = (from player in dbm.Players select player).Where(o => o.ID == id).First();
            return (int)tempPlayer.CurrentPosition;
                
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using PdfSharp.Pdf;
using PdfSharp.Fonts;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;






namespace TomaszMichalSzandalaLab1
{
    public partial class OrderForm : Form
    {
        ArrayList heroes = new ArrayList();
        HashSet<Hero> chosenHeroes = new HashSet<Hero>();
        HashSet<Skin> chosenSkins = new HashSet<Skin>();
        Hero heroCheaper;
        Skin skinCheaper;

        public OrderForm()
        {
            InitializeComponent();
            createHeroes();

            foreach (Hero hero in heroes)
            {
                comboBoxHeroes.Items.Add(hero);
            }
            // Associate the event-handling method with the 
            // SelectedIndexChanged event.
            this.comboBoxHeroes.SelectedIndexChanged +=
                new System.EventHandler(comboBoxHeroes_SelectedIndexChanged);

            Random rnd = new Random();
            heroCheaper = (Hero)heroes[rnd.Next(heroes.Count)];
            heroCheaper.price *= 0.5;
            skinCheaper = (Skin)heroCheaper.skins[rnd.Next(heroCheaper.skins.Count)];
            skinCheaper.price *= 0.5;

            labelHeroPromotion.Text = heroCheaper + " z " + skinCheaper;

        }

        private void comboBoxHeroes_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkedListBoxSkins.Items.Clear();

            Hero hero = (Hero)comboBoxHeroes.SelectedItem;
            foreach (Skin skin in hero.skins)
            {
                checkedListBoxSkins.Items.Add(skin);
            }
            checkedListBoxSkins.Enabled = true;
        }


        void createHeroes()
        {
            // ugly things - but I don't know how to use database

            Hero hero = new Hero("Sylvanas", 10.0);
            hero.addSkin(new Skin("Ranger General", 5.0));
            hero.addSkin(new Skin("Succub Sylvanas", 3.0));
            heroes.Add(hero);

            hero = new Hero("Valla", 0.5);
            hero.addSkin(new Skin("Angel", 2.0));
            hero.addSkin(new Skin("Queen Valla", 6.0));
            heroes.Add(hero);

            hero = new Hero("Tyrael", 7);
            hero.addSkin(new Skin("Devil Tyrael", 2.8));
            hero.addSkin(new Skin("Bloody Angel", 10.0));
            hero.addSkin(new Skin("Human Tyrael", 4.0));
            heroes.Add(hero);

            hero = new Hero("Tassadar", 6);
            hero.addSkin(new Skin("Mech Tassadar", 10.0));
            hero.addSkin(new Skin("Tribal Tass", 4));
            heroes.Add(hero);
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {

            chosenHeroes.Add((Hero)comboBoxHeroes.SelectedItem);
            foreach (Skin skin in checkedListBoxSkins.CheckedItems)
            {
                chosenSkins.Add(skin);
            }

            updateSummaries();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            chosenHeroes.Clear();
            chosenSkins.Clear();

            textBoxOrderSummary.Text = "";
            textBoxSum.Text = "Razem: 0.00";
        }

        private void buttonChoosePromotion_Click(object sender, EventArgs e)
        {
            chosenHeroes.Add(heroCheaper);
            chosenSkins.Add(skinCheaper);

            updateSummaries();
        }

        private void updateSummaries()
        {
            String summary = "";
            double sum = 0.0;
            foreach (Hero hero in chosenHeroes)
            {
                summary += hero + Environment.NewLine;
                sum += hero.price;
            }
            foreach (Skin skin in chosenSkins)
            {
                summary += skin + Environment.NewLine;
                sum += skin.price;
            }
            textBoxOrderSummary.Text = summary;
            textBoxSum.Text = "Razem: " + string.Format(" {0:N2}$ ", sum.ToString());

            checkedListBoxSkins.ClearSelected(); // nie dziala??
            checkedListBoxSkins.Update();
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            PdfDocument pdf = new PdfDocument();
            PdfPage pdfPage = pdf.AddPage();
            XFont font = new XFont("Verdana", 20, XFontStyle.Bold);

            XGraphics graph = XGraphics.FromPdfPage(pdfPage);
            XTextFormatter tf = new XTextFormatter(graph);
            XRect rect = new XRect(35, 35, pdfPage.Width-35, pdfPage.Height-35);

            String order = "Zamowienie:\n" + textBoxOrderSummary.Text;
            order += textBoxSum.Text;
            order += "\n\nDziękujemy za skorzystanie z naszych usług!";

            tf.DrawString(order, font, XBrushes.RoyalBlue, rect);

            DateTime today = DateTime.Now;
            pdf.Save("Zamowienie_" + today.ToString("yyyy-MM-dd_HH-mm") + ".pdf");

            MessageBox.Show("Zamówienie zapisano z datą: " + today.ToString("yyyy-MM-dd_HH-mm"), "Potwierdzenie");
            this.Close();
           
        }

        private void buttonCancelOrder_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }

    public class Hero
    {
        public String name { get; set; }
        public double price { get; set; }
        public ArrayList skins = new ArrayList();
        public void addSkin(Skin skin)
        {
            skins.Add(skin);
        }

        public Hero(String _name, double _price)
        {
            name = _name;
            price = _price;
        }

        public override String ToString()
        {
            return name + string.Format(" ( {0:N2}$ )", price);
        }
    }

    public class Skin
    {
        public String name { get; set; }
        public double price { get; set; }

        public Skin(String _name, double _price)
        {
            name = _name;
            price = _price;
        }
        public override string ToString()
        {
            return name + string.Format(" ( {0:N2}$ )", price);
        }

    }

}

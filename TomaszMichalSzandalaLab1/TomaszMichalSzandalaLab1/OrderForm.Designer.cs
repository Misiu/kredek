﻿namespace TomaszMichalSzandalaLab1
{
    partial class OrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelPromotionTitle = new System.Windows.Forms.Label();
            this.labelHeroPromotion = new System.Windows.Forms.Label();
            this.buttonChoosePromotion = new System.Windows.Forms.Button();
            this.comboBoxHeroes = new System.Windows.Forms.ComboBox();
            this.checkedListBoxSkins = new System.Windows.Forms.CheckedListBox();
            this.textBoxOrderSummary = new System.Windows.Forms.TextBox();
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.buttonCancelOrder = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.textBoxSum = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Lucida Sans Unicode", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.Location = new System.Drawing.Point(111, 9);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(396, 34);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "Heroes of the Storm - eShop";
            // 
            // labelPromotionTitle
            // 
            this.labelPromotionTitle.AutoSize = true;
            this.labelPromotionTitle.Font = new System.Drawing.Font("Lucida Sans Unicode", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromotionTitle.Location = new System.Drawing.Point(12, 73);
            this.labelPromotionTitle.Name = "labelPromotionTitle";
            this.labelPromotionTitle.Size = new System.Drawing.Size(180, 25);
            this.labelPromotionTitle.TabIndex = 1;
            this.labelPromotionTitle.Text = "Promocja chwili:";
            // 
            // labelHeroPromotion
            // 
            this.labelHeroPromotion.AutoSize = true;
            this.labelHeroPromotion.Font = new System.Drawing.Font("Lucida Sans Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHeroPromotion.Location = new System.Drawing.Point(198, 77);
            this.labelHeroPromotion.Name = "labelHeroPromotion";
            this.labelHeroPromotion.Size = new System.Drawing.Size(58, 20);
            this.labelHeroPromotion.TabIndex = 2;
            this.labelHeroPromotion.Text = "label1";
            // 
            // buttonChoosePromotion
            // 
            this.buttonChoosePromotion.Font = new System.Drawing.Font("Lucida Sans Unicode", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonChoosePromotion.Location = new System.Drawing.Point(602, 77);
            this.buttonChoosePromotion.Name = "buttonChoosePromotion";
            this.buttonChoosePromotion.Size = new System.Drawing.Size(105, 31);
            this.buttonChoosePromotion.TabIndex = 4;
            this.buttonChoosePromotion.Text = "Wybierz";
            this.buttonChoosePromotion.UseVisualStyleBackColor = true;
            this.buttonChoosePromotion.Click += new System.EventHandler(this.buttonChoosePromotion_Click);
            // 
            // comboBoxHeroes
            // 
            this.comboBoxHeroes.FormattingEnabled = true;
            this.comboBoxHeroes.Location = new System.Drawing.Point(17, 180);
            this.comboBoxHeroes.Name = "comboBoxHeroes";
            this.comboBoxHeroes.Size = new System.Drawing.Size(142, 21);
            this.comboBoxHeroes.TabIndex = 5;
            this.comboBoxHeroes.Text = "Wybierz bohatera";
            // 
            // checkedListBoxSkins
            // 
            this.checkedListBoxSkins.Enabled = false;
            this.checkedListBoxSkins.FormattingEnabled = true;
            this.checkedListBoxSkins.Location = new System.Drawing.Point(177, 180);
            this.checkedListBoxSkins.Name = "checkedListBoxSkins";
            this.checkedListBoxSkins.Size = new System.Drawing.Size(154, 94);
            this.checkedListBoxSkins.TabIndex = 6;
            // 
            // textBoxOrderSummary
            // 
            this.textBoxOrderSummary.Location = new System.Drawing.Point(448, 146);
            this.textBoxOrderSummary.Multiline = true;
            this.textBoxOrderSummary.Name = "textBoxOrderSummary";
            this.textBoxOrderSummary.ReadOnly = true;
            this.textBoxOrderSummary.Size = new System.Drawing.Size(158, 128);
            this.textBoxOrderSummary.TabIndex = 7;
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.Font = new System.Drawing.Font("Lucida Sans Unicode", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSubmit.Location = new System.Drawing.Point(393, 347);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(114, 66);
            this.buttonSubmit.TabIndex = 8;
            this.buttonSubmit.Text = "Dokonaj zakupu";
            this.buttonSubmit.UseVisualStyleBackColor = true;
            this.buttonSubmit.Click += new System.EventHandler(this.buttonSubmit_Click);
            // 
            // buttonCancelOrder
            // 
            this.buttonCancelOrder.Font = new System.Drawing.Font("Lucida Sans Unicode", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancelOrder.Location = new System.Drawing.Point(88, 347);
            this.buttonCancelOrder.Name = "buttonCancelOrder";
            this.buttonCancelOrder.Size = new System.Drawing.Size(114, 66);
            this.buttonCancelOrder.TabIndex = 9;
            this.buttonCancelOrder.Text = "Anuluj";
            this.buttonCancelOrder.UseVisualStyleBackColor = true;
            this.buttonCancelOrder.Click += new System.EventHandler(this.buttonCancelOrder_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(337, 180);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(75, 23);
            this.buttonAdd.TabIndex = 10;
            this.buttonAdd.Text = "Dodaj";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(337, 232);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 23);
            this.buttonClear.TabIndex = 11;
            this.buttonClear.Text = "Wyczyść";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // textBoxSum
            // 
            this.textBoxSum.Location = new System.Drawing.Point(448, 301);
            this.textBoxSum.Name = "textBoxSum";
            this.textBoxSum.ReadOnly = true;
            this.textBoxSum.Size = new System.Drawing.Size(158, 20);
            this.textBoxSum.TabIndex = 12;
            this.textBoxSum.Text = "Razem:";
            // 
            // OrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 425);
            this.Controls.Add(this.textBoxSum);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.buttonCancelOrder);
            this.Controls.Add(this.buttonSubmit);
            this.Controls.Add(this.textBoxOrderSummary);
            this.Controls.Add(this.checkedListBoxSkins);
            this.Controls.Add(this.comboBoxHeroes);
            this.Controls.Add(this.buttonChoosePromotion);
            this.Controls.Add(this.labelHeroPromotion);
            this.Controls.Add(this.labelPromotionTitle);
            this.Controls.Add(this.labelTitle);
            this.Name = "OrderForm";
            this.Text = "OrderForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelPromotionTitle;
        private System.Windows.Forms.Label labelHeroPromotion;
        private System.Windows.Forms.Button buttonChoosePromotion;
        private System.Windows.Forms.ComboBox comboBoxHeroes;
        private System.Windows.Forms.CheckedListBox checkedListBoxSkins;
        private System.Windows.Forms.TextBox textBoxOrderSummary;
        private System.Windows.Forms.Button buttonSubmit;
        private System.Windows.Forms.Button buttonCancelOrder;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.TextBox textBoxSum;
    }
}
﻿using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TomaszMichalSzandalaLab2.Model;
namespace TomaszMichalSzandalaLab2.Controler
{
    public static class VotingManager
    {
        private static void prepareVotingResults(ref List<Candidate> listOfCandidates)
        {
            listOfCandidates.Find(o => o.komor).votes += votesNotForKomor(ref listOfCandidates) + 1;

            listOfCandidates.OrderByDescending(o => o.votes);
        }

        private static int votesNotForKomor(ref List<Candidate> listOfCandidates)
        {
            int votesCount = 0;
            foreach (var candidate in listOfCandidates)
            {   if(!candidate.komor)
                    votesCount += candidate.votes;
            }
            return votesCount;
        }
        private static int sumVotes(ref List<Candidate> listOfCandidates)
        {
            int sum = 0;
            foreach (var candidate in listOfCandidates)
            {
                sum += candidate.votes;
            }
            return sum;
        }

        public static void writeToPDF(ref List<Candidate> listOfCandidates)
        {
            prepareVotingResults(ref listOfCandidates);
            int sumOfVotes=sumVotes(ref listOfCandidates);


            PdfDocument pdf = new PdfDocument();
            PdfPage pdfPage = pdf.AddPage();
            XFont font = new XFont("Verdana", 17, XFontStyle.Regular);

            XGraphics graph = XGraphics.FromPdfPage(pdfPage);
            XTextFormatter tf = new XTextFormatter(graph);
            XRect rect = new XRect(35, 35, pdfPage.Width - 35, pdfPage.Height - 35);

            String results = "Podsumowanie wyborów prezydenckich 2015 - I tura." + Environment.NewLine;
            results += "Kandydaci uzyskali wyniki:" + Environment.NewLine;
            foreach (var candidate in listOfCandidates)
            {
                results += candidate.name + string.Format(" uzyskał {0:N2}% głosów", (candidate.votes / (double)sumOfVotes)*100.0) + Environment.NewLine;
            }


            tf.DrawString(results, font, XBrushes.RoyalBlue, rect);

            DateTime today = DateTime.Now;
            pdf.Save("Wybory 2015.pdf");

            //MessageBox.Show("Zamówienie zapisano z datą: " + today.ToString("yyyy-MM-dd_HH-mm"), "Potwierdzenie");
          


        }
    }
}

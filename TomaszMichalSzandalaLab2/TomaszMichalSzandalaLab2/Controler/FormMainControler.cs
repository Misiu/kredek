﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace TomaszMichalSzandalaLab2.Controler
{
    public static class FormMainControler
    {
        /// <summary>
        /// WooW! My first magic function.
        /// </summary>
        /// <param name="dataGridViewArrivals">GridView with Arrivals data</param>
        public static void WriteToFile(DataGridView dataGridViewArrivals)
        {
            StreamWriter streamWriter = new StreamWriter("tabelka.txt");

            foreach (DataGridViewRow row in dataGridViewArrivals.Rows)
            {
                streamWriter.WriteLine(row.Cells[0].Value + ";" + row.Cells[1].Value + ";" + row.Cells[2].Value + ";");
            }
            streamWriter.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TomaszMichalSzandalaLab2.Model
{
    public static class CandidatesManager
    {
        /// <summary>
        /// Reads list of candidates from a file
        /// </summary>
        /// <returns> List\<Candidate\></returns>
        public static List<Candidate> readCandidates()
        {
            List<Candidate> listOfCandidates = new List<Candidate>();
            Candidate candidate;

            foreach (String line in File.ReadAllLines(".." + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + "kandydaci.txt"))
            {
                candidate = new Candidate();
                var details = line.Split('#');
                candidate.name = details[0];
                candidate.description = details[1];

                if (candidate.name.Contains("\"BUL\""))
                    candidate.komor = true;

                listOfCandidates.Add(candidate);
            }

            return listOfCandidates;

        }
    }
}

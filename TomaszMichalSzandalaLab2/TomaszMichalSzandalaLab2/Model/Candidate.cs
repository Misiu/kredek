﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TomaszMichalSzandalaLab2.Model
{
    public class Candidate
    {
        public String name { get; set; }
        public String description { get; set; }
        public int votes { get; set; }
        public Boolean komor { get; set; }

        public override String ToString(){
            return name;
        }
        public Candidate()
        {
            votes = 0;
            komor = false;
        }

    }
}

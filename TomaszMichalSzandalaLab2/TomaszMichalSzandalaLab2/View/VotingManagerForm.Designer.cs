﻿namespace TomaszMichalSzandalaLab2.View
{
    partial class VotingManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonToVote = new System.Windows.Forms.Button();
            this.buttonSumUp = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonToVote
            // 
            this.buttonToVote.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonToVote.Location = new System.Drawing.Point(85, 58);
            this.buttonToVote.Name = "buttonToVote";
            this.buttonToVote.Size = new System.Drawing.Size(184, 106);
            this.buttonToVote.TabIndex = 0;
            this.buttonToVote.Text = "Zagłosuj";
            this.buttonToVote.UseVisualStyleBackColor = true;
            this.buttonToVote.Click += new System.EventHandler(this.buttonToVote_Click);
            // 
            // buttonSumUp
            // 
            this.buttonSumUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSumUp.Location = new System.Drawing.Point(85, 257);
            this.buttonSumUp.Name = "buttonSumUp";
            this.buttonSumUp.Size = new System.Drawing.Size(184, 106);
            this.buttonSumUp.TabIndex = 1;
            this.buttonSumUp.Text = "Podsumuj wybory";
            this.buttonSumUp.UseVisualStyleBackColor = true;
            this.buttonSumUp.Click += new System.EventHandler(this.buttonSumUp_Click);
            // 
            // VotingManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 453);
            this.Controls.Add(this.buttonSumUp);
            this.Controls.Add(this.buttonToVote);
            this.Name = "VotingManagerForm";
            this.Text = "VotingManagerForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonToVote;
        private System.Windows.Forms.Button buttonSumUp;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TomaszMichalSzandalaLab2.Model;

namespace TomaszMichalSzandalaLab2.View
{
    public partial class VotingForm : Form
    {
        public VotingForm(ref List<Candidate> listOfCandidates)
        {
            InitializeComponent();
            foreach (var candidate in listOfCandidates)
            {
                checkedListBoxCandidates.Items.Add(candidate);
            }

            this.checkedListBoxCandidates.ItemCheck +=
                new ItemCheckEventHandler(checkedListBoxCandidates_ItemCheck);
        }



        private void checkedListBoxCandidates_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            for (int i = 0; i < checkedListBoxCandidates.Items.Count; i++)
                if (i != e.Index)
                    checkedListBoxCandidates.SetItemChecked(i, false);
            
            textBoxCandidateDescription.Text = ((Candidate)checkedListBoxCandidates.Items[e.Index]).description;
        }

        private void buttonVote_Click(object sender, EventArgs e)
        {
            ((Candidate)checkedListBoxCandidates.CheckedItems[0]).votes += 1;
            Close();
        }


    }
}
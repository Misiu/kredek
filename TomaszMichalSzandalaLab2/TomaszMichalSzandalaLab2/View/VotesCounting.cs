﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TomaszMichalSzandalaLab2.Controler;
using TomaszMichalSzandalaLab2.Model;

namespace TomaszMichalSzandalaLab2.View
{
    public partial class VotesCounting : Form
    {
        BackgroundWorker backgroundWorker;
        List<Candidate> listOfCandidates;
        public VotesCounting(List<Candidate> _listOfCandidates)
        {
            listOfCandidates = _listOfCandidates;
            InitializeComponent();
            backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
            backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
            backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker_runWorkerCompleted);


        }


        public void Message(String text)
        {
            labelOfProgress.Text = text;
        }

        public void ProgressValue(int value)
        {
            progressBar.Value = value;
        }


        private void VotesCounting_Load(object sender, System.EventArgs e)
        {
            // Start the BackgroundWorker.
            backgroundWorker.RunWorkerAsync();


        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Create the thread object, passing in the Alpha.Beta method
            // via a ThreadStart delegate. This does not start the thread.
            Thread writer = new Thread(() => VotingManager.writeToPDF(ref listOfCandidates));
            writer.Start();

            backgroundWorker.WorkerReportsProgress = true;
            for (int i = 1; i <= 100; i++)
            {
                // Wait 100 milliseconds.
                System.Threading.Thread.Sleep(300);
                // Report progress.
                backgroundWorker.ReportProgress(i);
                //progressBar.ForeColor = Color.RoyalBlue;
                //progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            }
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // Change the value of the ProgressBar to the BackgroundWorker progress.
            progressBar.Value = e.ProgressPercentage;
            // Set the text.
            this.Text = e.ProgressPercentage.ToString();
        }
        private void backgroundWorker_runWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            Close();
        }
    }
}

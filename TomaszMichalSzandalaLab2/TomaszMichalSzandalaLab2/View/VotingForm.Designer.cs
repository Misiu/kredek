﻿namespace TomaszMichalSzandalaLab2.View
{
    partial class VotingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkedListBoxCandidates = new System.Windows.Forms.CheckedListBox();
            this.textBoxCandidateDescription = new System.Windows.Forms.TextBox();
            this.buttonVote = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // checkedListBoxCandidates
            // 
            this.checkedListBoxCandidates.CheckOnClick = true;
            this.checkedListBoxCandidates.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkedListBoxCandidates.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkedListBoxCandidates.FormattingEnabled = true;
            this.checkedListBoxCandidates.Location = new System.Drawing.Point(63, 19);
            this.checkedListBoxCandidates.Name = "checkedListBoxCandidates";
            this.checkedListBoxCandidates.Size = new System.Drawing.Size(334, 292);
            this.checkedListBoxCandidates.TabIndex = 0;
            // 
            // textBoxCandidateDescription
            // 
            this.textBoxCandidateDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCandidateDescription.Location = new System.Drawing.Point(63, 357);
            this.textBoxCandidateDescription.Multiline = true;
            this.textBoxCandidateDescription.Name = "textBoxCandidateDescription";
            this.textBoxCandidateDescription.Size = new System.Drawing.Size(334, 127);
            this.textBoxCandidateDescription.TabIndex = 1;
            // 
            // buttonVote
            // 
            this.buttonVote.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonVote.Location = new System.Drawing.Point(63, 501);
            this.buttonVote.Name = "buttonVote";
            this.buttonVote.Size = new System.Drawing.Size(334, 37);
            this.buttonVote.TabIndex = 2;
            this.buttonVote.Text = "Daj głos";
            this.buttonVote.UseVisualStyleBackColor = true;
            this.buttonVote.Click += new System.EventHandler(this.buttonVote_Click);
            // 
            // VotingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 557);
            this.Controls.Add(this.buttonVote);
            this.Controls.Add(this.textBoxCandidateDescription);
            this.Controls.Add(this.checkedListBoxCandidates);
            this.Name = "VotingForm";
            this.Text = "VotingForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox checkedListBoxCandidates;
        private System.Windows.Forms.TextBox textBoxCandidateDescription;
        private System.Windows.Forms.Button buttonVote;
    }
}
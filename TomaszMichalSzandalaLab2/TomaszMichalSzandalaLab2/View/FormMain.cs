﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TomaszMichalSzandalaLab2.Controler;
using TomaszMichalSzandalaLab2.View;

namespace TomaszMichalSzandalaLab2
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {

            dataGridViewArrivals.Rows.Add(textBoxRegister.Text, textBoxSupply.Text, textBoxQuantity.Text);

        }

        private void buttonSaveFile_Click(object sender, EventArgs e)
        {
            FormMainControler.WriteToFile(dataGridViewArrivals);
        }

        private void buttonGoVoting_Click(object sender, EventArgs e)
        {
            VotingManagerForm votingManagerForm = new VotingManagerForm();
            votingManagerForm.ShowDialog();
            Close();
        }

        //TODO
        //zad 1 Odcyt z pliku
        // filtrowanie w gridview'ie
        //zad 2 z gwiazdką
        // + 2 okna + progress bar
        // Timer (ustawić)
        // ...



        
    }
}

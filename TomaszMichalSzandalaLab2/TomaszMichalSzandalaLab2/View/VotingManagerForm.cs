﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TomaszMichalSzandalaLab2.Model;

namespace TomaszMichalSzandalaLab2.View
{
    public partial class VotingManagerForm : Form
    {
        List<Candidate> listOfCandidates;
        public VotingManagerForm()
        {
            InitializeComponent();
            listOfCandidates = CandidatesManager.readCandidates();

        }

        private void buttonToVote_Click(object sender, EventArgs e)
        {
            VotingForm votingForm = new VotingForm(ref listOfCandidates);
            votingForm.ShowDialog();
        }

        private void buttonSumUp_Click(object sender, EventArgs e)
        {
            VotesCounting votesCounting=new VotesCounting(listOfCandidates);
            
            votesCounting.ShowDialog();
            Close();
        }
    }
}

﻿namespace TomaszMichalSzandalaLab2
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonNew = new System.Windows.Forms.Button();
            this.labelRegister = new System.Windows.Forms.Label();
            this.labelSupply = new System.Windows.Forms.Label();
            this.labelQuantity = new System.Windows.Forms.Label();
            this.textBoxRegister = new System.Windows.Forms.TextBox();
            this.textBoxSupply = new System.Windows.Forms.TextBox();
            this.textBoxQuantity = new System.Windows.Forms.TextBox();
            this.dataGridViewArrivals = new System.Windows.Forms.DataGridView();
            this.colRegister = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSupply = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonSaveFile = new System.Windows.Forms.Button();
            this.buttonGoVoting = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArrivals)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonNew
            // 
            this.buttonNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNew.Location = new System.Drawing.Point(54, 36);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(117, 64);
            this.buttonNew.TabIndex = 0;
            this.buttonNew.Text = "Nowy przyjazd";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // labelRegister
            // 
            this.labelRegister.AutoSize = true;
            this.labelRegister.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRegister.Location = new System.Drawing.Point(248, 36);
            this.labelRegister.Name = "labelRegister";
            this.labelRegister.Size = new System.Drawing.Size(162, 25);
            this.labelRegister.TabIndex = 1;
            this.labelRegister.Text = "Nr rejestracyjny";
            // 
            // labelSupply
            // 
            this.labelSupply.AutoSize = true;
            this.labelSupply.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSupply.Location = new System.Drawing.Point(469, 36);
            this.labelSupply.Name = "labelSupply";
            this.labelSupply.Size = new System.Drawing.Size(71, 25);
            this.labelSupply.TabIndex = 2;
            this.labelSupply.Text = "Towar";
            // 
            // labelQuantity
            // 
            this.labelQuantity.AutoSize = true;
            this.labelQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQuantity.Location = new System.Drawing.Point(673, 36);
            this.labelQuantity.Name = "labelQuantity";
            this.labelQuantity.Size = new System.Drawing.Size(56, 25);
            this.labelQuantity.TabIndex = 3;
            this.labelQuantity.Text = "Ilość";
            // 
            // textBoxRegister
            // 
            this.textBoxRegister.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRegister.Location = new System.Drawing.Point(266, 86);
            this.textBoxRegister.Name = "textBoxRegister";
            this.textBoxRegister.Size = new System.Drawing.Size(100, 26);
            this.textBoxRegister.TabIndex = 4;
            this.textBoxRegister.Text = "ONY 4PF6";
            // 
            // textBoxSupply
            // 
            this.textBoxSupply.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSupply.Location = new System.Drawing.Point(445, 86);
            this.textBoxSupply.Name = "textBoxSupply";
            this.textBoxSupply.Size = new System.Drawing.Size(126, 26);
            this.textBoxSupply.TabIndex = 5;
            this.textBoxSupply.Text = "Krzesło bulu";
            // 
            // textBoxQuantity
            // 
            this.textBoxQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxQuantity.Location = new System.Drawing.Point(644, 86);
            this.textBoxQuantity.Name = "textBoxQuantity";
            this.textBoxQuantity.Size = new System.Drawing.Size(100, 26);
            this.textBoxQuantity.TabIndex = 6;
            this.textBoxQuantity.Text = "1";
            this.textBoxQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dataGridViewArrivals
            // 
            this.dataGridViewArrivals.AllowUserToAddRows = false;
            this.dataGridViewArrivals.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewArrivals.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewArrivals.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewArrivals.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colRegister,
            this.colSupply,
            this.colQuantity});
            this.dataGridViewArrivals.Location = new System.Drawing.Point(12, 144);
            this.dataGridViewArrivals.Name = "dataGridViewArrivals";
            this.dataGridViewArrivals.Size = new System.Drawing.Size(794, 212);
            this.dataGridViewArrivals.TabIndex = 7;
            // 
            // colRegister
            // 
            this.colRegister.HeaderText = "Nr rejestracyjny";
            this.colRegister.Name = "colRegister";
            // 
            // colSupply
            // 
            this.colSupply.HeaderText = "Towar";
            this.colSupply.Name = "colSupply";
            // 
            // colQuantity
            // 
            this.colQuantity.HeaderText = "Ilość";
            this.colQuantity.Name = "colQuantity";
            // 
            // buttonSaveFile
            // 
            this.buttonSaveFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSaveFile.Location = new System.Drawing.Point(295, 376);
            this.buttonSaveFile.Name = "buttonSaveFile";
            this.buttonSaveFile.Size = new System.Drawing.Size(205, 42);
            this.buttonSaveFile.TabIndex = 8;
            this.buttonSaveFile.Text = "Zapisz do pliku";
            this.buttonSaveFile.UseVisualStyleBackColor = true;
            this.buttonSaveFile.Click += new System.EventHandler(this.buttonSaveFile_Click);
            // 
            // buttonGoVoting
            // 
            this.buttonGoVoting.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGoVoting.Location = new System.Drawing.Point(668, 404);
            this.buttonGoVoting.Name = "buttonGoVoting";
            this.buttonGoVoting.Size = new System.Drawing.Size(153, 35);
            this.buttonGoVoting.TabIndex = 9;
            this.buttonGoVoting.Text = "Idź głosować";
            this.buttonGoVoting.UseVisualStyleBackColor = true;
            this.buttonGoVoting.Click += new System.EventHandler(this.buttonGoVoting_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 439);
            this.Controls.Add(this.buttonGoVoting);
            this.Controls.Add(this.buttonSaveFile);
            this.Controls.Add(this.dataGridViewArrivals);
            this.Controls.Add(this.textBoxQuantity);
            this.Controls.Add(this.textBoxSupply);
            this.Controls.Add(this.textBoxRegister);
            this.Controls.Add(this.labelQuantity);
            this.Controls.Add(this.labelSupply);
            this.Controls.Add(this.labelRegister);
            this.Controls.Add(this.buttonNew);
            this.Name = "FormMain";
            this.Text = "Rejestr przyjazdów";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArrivals)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.Label labelRegister;
        private System.Windows.Forms.Label labelSupply;
        private System.Windows.Forms.Label labelQuantity;
        private System.Windows.Forms.TextBox textBoxRegister;
        private System.Windows.Forms.TextBox textBoxSupply;
        private System.Windows.Forms.TextBox textBoxQuantity;
        private System.Windows.Forms.DataGridView dataGridViewArrivals;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRegister;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSupply;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQuantity;
        private System.Windows.Forms.Button buttonSaveFile;
        private System.Windows.Forms.Button buttonGoVoting;
    }
}

